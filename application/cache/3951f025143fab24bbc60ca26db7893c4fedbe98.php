<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('css'); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo e(base_url('assets')); ?>/plugins/iCheck/all.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo e(base_url('assets')); ?>/bower_components/select2/dist/css/select2.min.css">
<style type="text/css">
  .judul{
    font-size: 25px;
    font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  }

  .desc{
    font-size: 18px;
    font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    font-style: italic;
    margin-bottom: 30px
  }
  .inf{
    font-size: 10px
  }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Blank page
      <small>it all starts here</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo e(base_url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo e(base_url('admin/lahan')); ?>">List Lahan</a></li>
      <li class="active">Data Lahan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body box-profile">
          <center>
            <img width="350px" style="margin-bottom: 0px; margin-top: 10px" class="img-responsive" src="<?php echo e($data['imagedir']); ?>"" alt="User profile picture">
            <span class="judul"><?php echo e($data['judul']); ?></span>
            <p class="desc">"<?php echo e($data['deskripsi']); ?>"</p>
          </center>
          <div class="col-md-6">
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Harga</b> <a class="pull-right"><?php echo e($data["harga"]); ?></a>
              </li>
              
              <li class="list-group-item">
                <b>Irigasi</b> <a class="pull-right"><?php echo e($data['fasilitas_irigasi']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Tanah</b> <a class="pull-right"><?php echo e($data['fasilitas_tanah']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Jalan</b> <a class="pull-right"><?php echo e($data['fasilitas_jalan']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Pemandangan</b> <a class="pull-right"><?php echo e($data['fasilitas_pemandangan']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Status</b> <a class="pull-right">

                  <?php if($data['status'] == 'true'): ?>
                    <span style="font-size: 12px" class="label label-success">Terverifikasi</span>
                  <?php elseif($data['status'] == 'false'): ?>
                    <span style="font-size: 12px" class="label label-primary">Belum Terverifikasi</span> 
                  <?php else: ?>
                    <span style="font-size: 12px" class="label label-danger">Diblokir</span>
                  <?php endif; ?>

                </a>
              </li>
              <li class="list-group-item"><center>
                  <?php if($data['status'] == 'true'): ?>
                    
                      
                      <button data-toggle="modal" data-target="#modal-warning" type="submit" style="font-size: 16px" class="label btn btn-danger">Ubah Data</button>
                    
                    <div class="modal fade" id="modal-warning">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">PERINGATAN PENGUBAHAN DATA LAHAN</h4>
                        </div>
                        <div class="modal-body">
                          <div class="box-body">
                          <p>Pilih opsi di bawah ini atau Lewati tanpa memilih.</p><br>
                          <form id="form1" action="<?php echo e($data->Urlupdate.'/'.$data->user->id); ?>" method="post">
                            
                            <div class="col-md-12">
                              <div class="form-group">
                                <div class="col-md-5">
                                  <!-- checkbox -->
                                    <input type="checkbox" class="minimal pull-left" name="alert" id="alert">
                                    <span class="label label-danger pull-left">Beri Peringatan</span><br>
                                    <?php if($data->user->alert <= 0): ?>
                                      <span class="inf">(User Ini Belum Mendapat Peringatan)</span>
                                    <?php elseif( $data->user->alert >= 1): ?>
                                      <span class="inf">(User Ini Mendapatkan <?php echo e($data->user->alert); ?> Kali Peringatan)</span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-7">
                                  <p style="font-size: 10px; text-align: left;" class="pull-left">*Berikan peringatan kepada user.<br> User akan diberi 1 peringatan. Dan Iklan otomatis <b>DIBATALKAN VERIFIKASINYA</b>.</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <div class="col-md-5">
                                  <!-- checkbox -->
                                    <input type="checkbox" class="minimal pull-left" name="delete">
                                    <span class="label label-danger pull-left">Hapus Lahan</span>
                                </div>
                                <div class="col-md-7">
                                  <p style="font-size: 10px" class="pull-left">*Iklan lahan akan <b>DIHAPUS</b>.</p>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <div class="col-md-5">
                                  <!-- checkbox -->
                                    <input type="checkbox" class="minimal pull-left" name="cancel">
                                    <span class="label label-danger pull-left">Batalkan Verifikasi Lahan</span>
                                </div>
                                <div class="col-md-7">
                                  <p style="font-size: 10px" class="pull-left">*Verifikasi Lahan Akan <b>DIBATALKAN</b>.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batalkan</button>
                          <button type="submit" class="btn btn-primary">Simpan Perubahan</button>

                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                  <?php elseif($data['status'] == 'false'): ?>
                    <form id="form" action="<?php echo e($data->Urlupdate); ?>" method="post">
                      <input type="text" hidden id="verif" name="verif" value="true">
                      <button type="submit" style="font-size: 16px" class="label btn btn-success">Verifikasi Lahan</button>
                    </form>
                  <?php else: ?>
                    <button type="submit" style="font-size: 16px" class="label btn btn-default">Tidak Dapat Verifikasi Lahan</button>
                  <?php endif; ?></center>
              </li>
            </ul>
          </div>

          <div class="col-md-6">
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Kategori</b> <a class="pull-right"><?php echo e($data["id_kategori"]); ?></a>
              </li>
              <li class="list-group-item">
                <b>Kontak Pemilik</b> <a class="pull-right">543</a>
              </li>
              <li class="list-group-item">
                <b>Luas</b> <a class="pull-right"><?php echo e($data['luas']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Setifikasi</b> <a class="pull-right"><?php echo e($data['sertifikasi']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Kurun Sewa</b> <a class="pull-right"><?php echo e($data['kurun_sewa']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Kondisi</b> <a class="pull-right"><?php echo e($data['kondisi']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Waktu Dibuat</b> <a class="pull-right"><?php echo e($data['fieldCreate_at']); ?></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<!-- iCheck 1.0.1 -->
<script src="<?php echo e(base_url('assets')); ?>/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo e(base_url('assets')); ?>/bower_components/fastclick/lib/fastclick.js"></script>
<script>
var base_update = "<?php echo e($data->Urlupdate); ?>";
var id_user = "/<?php echo e($data->user->id); ?>";

  $().ready(function(){
        $('#form').submit(function(e){
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                'url': base_update,
                'data': $(this).serialize(),
                'success': function(html){
                    swal({
                      title: 'Data Berhasil Di Ubah',
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
            });
        });
    });

  $().ready(function(){
        $('#form1').submit(function(e){
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                'url': base_update+id_user,
                'data': $(this).serialize(),
                'success': function(html){
                    swal({
                      title: 'Data Berhasil Di Ubah',
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
            });
        });
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>