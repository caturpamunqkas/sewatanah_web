<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pengguna
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(base_url("/admin")); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">List User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>NAMA LENGKAP</th>
                  <th>EMAIL</th>
                  <th>ALAMAT</th>
                  <th>TELEPON</th>
                  
                  <th>ALERT</th>
                  <th>STATUS VERIFIKASI</th>
                  <th>AKSI</th>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d => $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr align="center">
                  <td><?php echo e($d+1); ?></td>
                  <td><?php echo e($result['nama_depan']." ".$result['nama_belakang']); ?></td>
                  <td><?php echo e($result['email']); ?></td>
                  <td><?php echo e($result['alamat']); ?></td>
                  <td><?php echo e($result['telepon']); ?></td>
                  
                  <td align="center">
                    
                    <?php if($result['alert'] >= 5): ?>
                      <span class="label label-danger">BLOCK</span>
                    <?php elseif($result['alert'] >= 3): ?>
                      <span class="label" style="background-color: #f39c12">DIBEKUKAN</span>
                    <?php elseif($result['alert'] >= 1): ?>
                      <span class="label label-default"><?php echo e($result['alert']); ?> PERINGATAN</span>
                    <?php elseif($result['alert'] >= 0): ?>
                      <span class="label label-default">BELUM ADA</span>
                    <?php endif; ?>
                  </td>
                  <td>
                    <?php if($result['verif'] == 'true'): ?>
                      <span class="label label-success">TERVERIFIKASI</span>
                    <?php elseif($result['verif'] == 'false'): ?>
                      <span class="label label-danger">BELUM TERVERIFIKASI</span>
                    <?php else: ?>
                      <span class="label label-default">PROSES VERIFIKASI</span>
                    <?php endif; ?>
                  </td>
                  <td>
                      <a href="<?php echo e($result->url); ?>"><button class="btn btn-success"><i class="fa fa-eye"></i></button></a>
                    <button type="button" class="btn btn-danger" onclick="deleteUser()"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<!-- DataTables -->
<script src="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  var base_url_admin = "<?php echo e(base_url("/admin")); ?>";
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  function deleteUser() {
    swal({
    title: 'Apakah Anda Yakin?',
    text: "Ingin Menghapus Data Ini!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus Ini!'
  }).then((result) => {
    if (result.value) {
      swal({
        title: 'Terhapus!',
        text: 'Dengan Anda Klik OK, Maka Data Akan Benar-Benar Terhapus.',
        type: 'success',
        confirmButtonText: 'OK'
      }).then((result) => {
        window.location = "<?php echo e($result->urldelete); ?>";
      });
    }
  })
  }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>