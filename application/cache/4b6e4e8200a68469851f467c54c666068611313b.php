<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('css'); ?>
	<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/dropzone/css/basic.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/dropzone/css/dropzone.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/summernote/summernote.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/summernote/summernote-bs3.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/lib/codemirror.css" />
		<link rel="stylesheet" href="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/theme/monokai.css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<header class="page-header">
	<h2>User Profile</h2>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Pages</span></li>
			<li><span>User Profile</span></li>
		</ol>

		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>

<!-- start: page -->

<div class="row">
	<div class="col-md-4 col-lg-3">

		<section class="panel">
			<div class="panel-body">
				<div class="thumb-info mb-md">
					<img width="190px" src="<?php echo e($data->imagedir); ?>" alt="<?php echo e($data->nama_depan.' '.$data->nama_belakang); ?>">
					<div class="thumb-info-title">
						<span class="thumb-info-inner"><?php echo e($data->nama_depan.' '.$data->nama_belakang); ?></span>
						<span class="thumb-info-type">
							<?php if($data['status'] == 'true'): ?>
							TERVERIFIKASI
							<?php else: ?>
							BELUM TERVERIFIKASI
							<?php endif; ?>
						</span>
					</div>
				</div>

				<div class="widget-toggle-expand mb-md">
					<div class="widget-header">
						<h6>Profile Completion</h6>
						<div class="widget-toggle">+</div>
					</div>
					<div class="widget-content-collapsed">
						<div class="progress progress-xs light">
							<?php if($data['foto'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>

							<?php elseif($data['gender'] != null && $data['telepon'] != null && $data['alamat'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
							
							<?php elseif($data['foto'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
							
							<?php elseif($data['foto'] != null && $data['gender'] != null && $data['telepon'] != null && $data['alamat'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
							
							<?php elseif($data['foto'] != null && $data['foto'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
							
							<?php elseif($data['foto'] != null && $data['foto'] != null && $data['gender'] != null && $data['telepon'] != null && $data['alamat'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
							
							<?php elseif($data['foto'] != null && $data['gender'] != null && $data['telepon'] != null && $data['alamat'] != null && $data['foto'] != null): ?>
							<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
							
							<?php endif; ?>
						</div>
					</div>
					<div class="widget-content-expanded">
						<ul class="simple-todo-list">
							<?php if($data['foto'] != null): ?>
							<li class="completed">Update Profile Picture</li>
							<?php else: ?>
							<li>Update Profile Picture</li>
							<?php endif; ?>
							<?php if($data['gender'] != null && $data['telepon'] != null && $data['alamat'] != null): ?>
							<li class="completed">Change Personal Information</li>
							<?php else: ?>
							<li>Change Personal Information</li>
							<?php endif; ?>

							<?php if($data['foto'] != null): ?>
							<li class="completed">Identity Verification</li>
							<?php else: ?>
							<li>Identity Verification</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>

				<hr class="dotted short">

				<div class="social-icons-list">
					<a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
					<a rel="tooltip" data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
					<a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
				</div>

			</div>
		</section>

	</div>
	<div class="col-md-8 col-lg-6">

		<div class="tabs">
			<ul class="nav nav-tabs tabs-primary">
				<li class="active">
					<a href="#overview" data-toggle="tab">Overview</a>
				</li>
				<li>
					<a href="#edit" data-toggle="tab">Ubah Data</a>
				</li>
				<li>
					<a href="#pass" data-toggle="tab">Ubah Password</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="overview" class="tab-pane ">
					Data User
				</div>
				<div id="edit" class="tab-pane active">

					<form class="form-horizontal" method="get">
						<h4 class="mb-xlg">Personal Information</h4>
						<fieldset>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileFirstName">Nama Depan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" value="<?php echo e($data['nama_depan']); ?>" id="profileFirstName">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileLastName">Nama Belakang</label>
								<div class="col-md-8">
									<input type="text" class="form-control" value="<?php echo e($data['nama_belakang']); ?>" id="profileLastName">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileAddress">Alamat</label>
								<div class="col-md-8">
									<input type="text" class="form-control" value="<?php echo e($data['alamat']); ?>" id="profileAddress">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileCompany">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly value="<?php echo e($data['email']); ?>" id="profileCompany">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileCompany">Telepon</label>
								<div class="col-md-8">
									<input type="text" class="form-control" value="<?php echo e($data['telepon']); ?>" id="profileCompany">
								</div>
							</div>
						</fieldset>
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</div>
							</div>
						</div>
					</form>
						<hr class="dotted tall">
					<form class="form-horizontal" method="post">
						<h4 class="mb-xlg">Verifikasi Identitas</h4>
						<fieldset>
							<section class="panel">
								<div class="panel-body" style="height: 200px">
									<form class="dropzone dz-square" id="dropzone-example"></form>
								</div>
							</section>
						</fieldset>
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div id="pass" class="tab-pane">

					<form class="form-horizontal" method="get">
						<h4 class="mb-xlg">Ubah Password</h4>
						<fieldset>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileNewPassword">Password Baru</label>
								<div class="col-md-8">
									<input type="password" placeholder="Password Baru" class="form-control" id="profileNewPassword">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileNewPasswordRepeat">Ulangi Password</label>
								<div class="col-md-8">
									<input type="password" placeholder="Konfirmasi Password Baru" class="form-control" id="profileNewPasswordRepeat">
								</div>
							</div>
							<hr class="dotted short">
							<div class="form-group">
								<label class="col-md-3 control-label" for="profileNewPasswordRepeat">Konfirmasi Password</label>
								<div class="col-md-8">
									<input type="password" placeholder="Password Saat Ini" class="form-control" id="profileNewPasswordRepeat">
								</div>
							</div>
						</fieldset>
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-lg-3">

		<h4 class="mb-md">Sale Stats</h4>
		<ul class="simple-card-list mb-xlg">
			
			<li class="primary">
				<p>Saldo Anda</p>
				<h3>Rp. 500.000</h3>
			</li>
			
		</ul>

		<h4 class="mb-md">Messages</h4>
		<ul class="simple-user-list mb-xlg">
			<li>
				<figure class="image rounded">
					<img src="<?php echo e(base_url('assets/user_akun')); ?>/images/!sample-user.jpg" alt="Joseph Doe Junior" class="img-circle">
				</figure>
				<span class="title">Joseph Doe Junior</span>
				<span class="message">Lorem ipsum dolor sit.</span>
			</li>
			<li>
				<figure class="image rounded">
					<img src="<?php echo e(base_url('assets/user_akun')); ?>/images/!sample-user.jpg" alt="Joseph Junior" class="img-circle">
				</figure>
				<span class="title">Joseph Junior</span>
				<span class="message">Lorem ipsum dolor sit.</span>
			</li>
			<li>
				<figure class="image rounded">
					<img src="<?php echo e(base_url('assets/user_akun')); ?>/images/!sample-user.jpg" alt="Joe Junior" class="img-circle">
				</figure>
				<span class="title">Joe Junior</span>
				<span class="message">Lorem ipsum dolor sit.</span>
			</li>
			<li>
				<figure class="image rounded">
					<img src="<?php echo e(base_url('assets/user_akun')); ?>/images/!sample-user.jpg" alt="Joseph Doe Junior" class="img-circle">
				</figure>
				<span class="title">Joseph Doe Junior</span>
				<span class="message">Lorem ipsum dolor sit.</span>
			</li>
		</ul>
	</div>

</div>
<!-- end: page -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<!-- Vendor -->
		
		<!-- Specific Page Vendor -->
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/select2/select2.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/fuelux/js/spinner.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/dropzone/dropzone.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-markdown/js/markdown.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/lib/codemirror.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/addon/selection/active-line.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/addon/edit/matchbrackets.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/mode/javascript/javascript.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/mode/xml/xml.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/codemirror/mode/css/css.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/summernote/summernote.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/ios7-switch/ios7-switch.js"></script>
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/vendor/bootstrap-confirmation/bootstrap-confirmation.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="<?php echo e(base_url('assets/user_akun')); ?>/javascripts/forms/examples.advanced.form.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>