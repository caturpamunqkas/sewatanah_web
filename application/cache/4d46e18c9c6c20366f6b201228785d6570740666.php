<?php $__env->startSection('content'); ?>
	<div role="main" class="main">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-md-push-3">
					<div class="row">
						<h1 class="h2 heading-primary font-weight-normal ml-md">Informasi Pribadi</h1>
						<div class="col-md-12">
							<form class="form-horizontal form-bordered">
								<div class="form-group">
									<label class="col-md-3 control-label" for="email" style="text-align: left;">Email</label>
									<div class="col-md-6">
										<input type="text" name="email" id="email" class="form-control" value="mrhabibiesmk@gmail.com" readonly>
									</div>
									<label class="col-md-3 control-label" style="text-align: left;"><a href="javascript:void(0)" data-toggle="modal" data-target="#changeEmail">Ubah Email</a></label>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="nohp" style="text-align: left;">Nomor Handphone</label>
									<div class="col-md-6">
										<input type="text" name="nohp" id="nohp" class="form-control" value="+6282143603556" readonly>
									</div>
									<label class="col-md-3 control-label" style="text-align: left;"><a href="javascript:void(0)" data-toggle="modal" data-target="#changePhone">Ubah Telepon</a></label>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="nama" style="text-align: left;">Nama <span class="required">*</span></label>
									<div class="col-md-6">
										<input type="text" name="nama" id="nama" class="form-control" value="Miftahur Rohman Habibie">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="gender" style="text-align: left;">Jenis Kelamin <span class="required">*</span></label>
									<div class="col-md-6">
										<label class="radio-inline">
											<input type="radio" name="gender" checked>
											Pria
										</label>
										<label class="radio-inline">
											<input type="radio" name="gender">
											Wanita
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="dob" style="text-align: left;">Tanggal Lahir <span class="required">*</span></label>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-4">
												<select class="form-control" id="dob">
													<option>Hari</option>
												</select>
											</div>
											<div class="col-md-4">
												<select class="form-control">
													<option>Bulan</option>
												</select>
											</div>
											<div class="col-md-4">
												<select class="form-control">
													<option>Tahun</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-3 col-md-offset-6">
										<button type="submit" class="btn btn-primary btn-block text-uppercase">Simpan</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<?php echo $__env->make('user.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>