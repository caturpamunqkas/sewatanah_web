<div class="col-md-3 col-md-pull-9">
	<aside class="sidebar">
		<h4><?php echo e($user->nama_depan.' '.$user->nama_belakang); ?></h4>
		<ul class="nav nav-list">
			<li class="<?php echo e(match($menu,'index','active')); ?>"><a href="<?php echo e(base_url('user')); ?>">Dashboard</a></li>
			<li class="<?php echo e(match($menu,'info','active')); ?>"><a href="<?php echo e(base_url('user/info')); ?>">Informasi Pribadi</a></li>
			<li class="<?php echo e(match($menu,'lahan','active')); ?>"><a href="<?php echo e(base_url('user/lahan')); ?>">Lahan Anda</a></li>
		</ul>
	</aside>
</div>