<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('css'); ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/owl.carousel/assets/owl.theme.default.min.css">
<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/rs-plugin/css/navigation.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/circle-flip-slideshow/css/component.css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/user/vendor/magnific-popup/magnific-popup.min.css">
<style type="text/css">
.carousel{
    background: #2f4357;
    margin-top: 2px;
}
.carousel .item{
    min-height: 280px; /* Prevent carousel from being distorted if for some reason image doesn't load */
}
.carousel .item img{
    margin: 0 auto; /* Align slide image horizontally center */
}
.bs-example{
	margin: 20px;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div role="main" class="main">
	<div class="slider-container rev_slider_wrapper" style="height: 700px;">
		<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 800, 'gridheight': 700}">
			<ul>
				<li data-transition="fade">
					<img src="<?php echo e(base_url("assets")); ?>/user/img/slides/landing-page-slide-1.jpg"  
						alt=""
						data-bgposition="center center" 
						data-bgfit="cover" 
						data-bgrepeat="no-repeat" 
						class="rev-slidebg">

					<div class="tp-caption"
						data-x="center" data-hoffset="-150"
						data-y="center" data-voffset="-95"
						data-start="1000"
						style="z-index: 5"
						data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<div class="tp-caption top-label"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="-95"
						data-start="500"
						style="z-index: 5"
						data-transform_in="y:[-300%];opacity:0;s:500;">DO YOU NEED A NEW</div>

					<div class="tp-caption"
						data-x="center" data-hoffset="150"
						data-y="center" data-voffset="-95"
						data-start="1000"
						style="z-index: 5"
						data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<div class="tp-caption main-label"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="-45"
						data-start="1500"
						data-whitespace="nowrap"						 
						data-transform_in="y:[100%];s:500;"
						data-transform_out="opacity:0;s:500;"
						style="z-index: 5"
						data-mask_in="x:0px;y:0px;">WEB DESIGN?</div>

					<div class="tp-caption bottom-label"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="5"
						data-start="2000"
						style="z-index: 5"
						data-transform_in="y:[100%];opacity:0;s:500;">Check out our options and features.</div>

					<a class="tp-caption btn btn-lg btn-primary btn-slider-action"
						data-hash
						data-hash-offset="85"
						href="#home-intro"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="80"
						data-start="2200"
						data-whitespace="nowrap"						 
						data-transform_in="y:[100%];s:500;"
						data-transform_out="opacity:0;s:500;"
						style="z-index: 5"
						data-mask_in="x:0px;y:0px;">Get Started Now!</a>
					
				</li>

				<li data-transition="fade">
					<img src="<?php echo e(base_url("assets")); ?>/user/video/landing.jpg"  
						alt=""
						data-bgposition="center center" 
						data-bgfit="cover" 
						data-bgrepeat="no-repeat" 
						class="rev-slidebg">

					<div class="rs-background-video-layer" 
						data-forcerewind="on" 
						data-volume="mute" 
						data-videowidth="100%" 
						data-videoheight="100%" 
						data-videomp4="video/landing.mp4" 
						data-videopreload="preload" 
						data-videoloop="none" 
						data-forceCover="1" 
						data-aspectratio="16:9" 
						data-autoplay="true" 
						data-autoplayonlyfirsttime="false" 
						data-nextslideatend="true" 
					></div>

					<div class="tp-caption"
						data-x="center" data-hoffset="-160"
						data-y="center" data-voffset="-95"
						data-start="1000"
						style="z-index: 5"
						data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<div class="tp-caption top-label"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="-95"
						data-start="500"
						style="z-index: 5"
						data-transform_in="y:[-300%];opacity:0;s:500;">START CREATING YOUR</div>

					<div class="tp-caption"
						data-x="center" data-hoffset="160"
						data-y="center" data-voffset="-95"
						data-start="1000"
						style="z-index: 5"
						data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<div class="tp-caption main-label"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="-45"
						data-start="1500"
						data-whitespace="nowrap"						 
						data-transform_in="y:[100%];s:500;"
						data-transform_out="opacity:0;s:500;"
						style="z-index: 5"
						data-mask_in="x:0px;y:0px;">NEW WEBSITE</div>

					<div class="tp-caption bottom-label"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="5"
						data-start="2000"
						style="z-index: 5"
						data-transform_in="y:[100%];opacity:0;s:500;">Join The 25,000+ Happy Customers :)</div>

					<a class="tp-caption btn btn-lg btn-primary btn-slider-action"
						data-hash
						data-hash-offset="85"
						href="#home-intro"
						data-x="center" data-hoffset="0"
						data-y="center" data-voffset="80"
						data-start="2200"
						data-whitespace="nowrap"						 
						data-transform_in="y:[100%];s:500;"
						data-transform_out="opacity:0;s:500;"
						style="z-index: 5"
						data-mask_in="x:0px;y:0px;">Get Started Now!</a>

					<div class="tp-dottedoverlay tp-opacity-overlay"></div>

				</li>
			</ul>
		</div>
	</div>
</div>
	<div class="container">
		<h2 class="title">
		<div align="center">&nbsp <span>Kategori</span> 
			<div class="row">     			
				<a href="#" class="btn btn-primary" style="padding: 20px;">Pertanian</a>
        		<a href="#" class="btn btn-primary" style="padding: 20px;">Perkebunan</a>
        		<a href="#" class="btn btn-primary" style="padding: 20px;">Hutan Produksi</a>
        		<a href="#" class="btn btn-primary" style="padding: 20px;">Toga</a>
        		<a href="#" class="btn btn-primary" style="padding: 20px;">Hidroponik</a>
        		<a href="#" class="btn btn-primary" style="padding: 20px;">Tanah Kosong</a>
        	</div>
		</div>
		</h2>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>