<?php $__env->startSection('title', 'Lihat Data '.$data['nama_depan'].' '.$data['nama_belakang']); ?>
<?php $__env->startSection('css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Data User
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(base_url("/admin")); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo e(base_url("/admin/listuser")); ?>">List User</a></li>
        <li class="active">Data User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title" style="text-align: center">DATA LAHAN</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="col-md-4">                  
                  <img width="300px" src="<?php echo e(base_url("assets/images/tanah_picture")); ?>/<?php echo e($data->lahan->foto_lahan); ?>">
                  <hr style="margin-top: 10px; margin-bottom: 10px">
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Lokasi</strong>
                  <p class="text-muted"><?php echo e($data->lahan->alamat_lahan); ?></p>
                </div>
                <div class="col-md-8">
                  <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                  <hr>
                  <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                  <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                  </p>
                  <hr>
                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="box">
              <div class="box-primary">
                
                <div class="box-header with-border box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?php echo e(base_url("assets/images/user_picture")); ?>/<?php echo e($data->pemilik->foto); ?>" alt="User profile picture">

                  <h3 class="profile-username text-center"><?php echo e($data->pemilik->nama_depan." ".$data->pemilik->nama_belakang); ?></h3>

                  <p class="text-muted text-center">Bergabung Sejak <?php echo e(tgl_indo($data->pemilik->created_at)); ?></p>

                  <ul class="list-group list-group-unbordered">
                    
                    <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right"><?php echo e($data->pemilik->email); ?></a>
                    </li>
                    
                    <li class="list-group-item">
                      <b>GENDER</b>
                      <?php if($data['gender'] == 'L'): ?>
                          <a class="pull-right">Laki - Laki</a>
                      <?php else: ?>
                          <a class="pull-right">Perempuan</a>
                      <?php endif; ?>
                    </li>
                    <li class="list-group-item">
                      <b>ALAMAT</b> <a class="pull-right"><?php echo e($data->pemilik->alamat); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>TELEPON</b> <a class="pull-right"><?php echo e($data->pemilik->telepon); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>PROFESI</b> <a class="pull-right"><?php echo e($data->pemilik->profesi); ?></a>
                    </li>
                    
                  </ul>

                  <a href="#" class="btn btn-success btn-block"><b>PEMILIK LAHAN</b></a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title" style="text-align: center">DATA LAHAN</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <img width="300px" src="<?php echo e(base_url("assets/images/tanah_picture")); ?>/<?php echo e($data->lahan->foto_lahan); ?>">
                <hr>
                <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>
                <hr>
                <strong><i class="fa fa-map-marker margin-r-5"></i> Lokasi</strong>
                <p class="text-muted">Malibu, California</p>
                <hr>
                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                <p>
                  <span class="label label-danger">UI Design</span>
                  <span class="label label-success">Coding</span>
                  <span class="label label-info">Javascript</span>
                  <span class="label label-warning">PHP</span>
                  <span class="label label-primary">Node.js</span>
                </p>
                <hr>
                <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="box">
              <div class="box-primary">
                <div class="box-header with-border box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?php echo e(base_url("assets/images/user_picture")); ?>/<?php echo e($data->penyewa->foto); ?>" alt="User profile picture">

                  <h3 class="profile-username text-center"><?php echo e($data->penyewa->nama_depan." ".$data->penyewa->nama_belakang); ?></h3>

                  <p class="text-muted text-center">Bergabung Sejak <?php echo e(tgl_indo($data->penyewa->created_at)); ?></p>

                  <ul class="list-group list-group-unbordered">
                    
                    <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right"><?php echo e($data->penyewa->email); ?></a>
                    </li>
                    
                    <li class="list-group-item">
                      <b>GENDER</b>
                      <?php if($data['gender'] == 'L'): ?>
                          <a class="pull-right">Laki - Laki</a>
                      <?php else: ?>
                          <a class="pull-right">Perempuan</a>
                      <?php endif; ?>
                    </li>
                    <li class="list-group-item">
                      <b>ALAMAT</b> <a class="pull-right"><?php echo e($data->penyewa->alamat); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>TELEPON</b> <a class="pull-right"><?php echo e($data->penyewa->telepon); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>PROFESI</b> <a class="pull-right"><?php echo e($data->penyewa->profesi); ?></a>
                    </li>
                    
                  </ul>

                  <a href="#" class="btn btn-success btn-block"><b>PENYEWA LAHAN</b></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>