<?php $__env->startSection('title', 'Panel Akun'); ?>

<?php $__env->startSection('content'); ?>
	<div role="main" class="main">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-push-3">
						<div class="row">
							<h1 class="h2 heading-primary font-weight-normal ml-md">Panel Akun</h1>
							<div class="col-md-4">
								<div class="panel-box">
									<div class="panel-box-title">
										<h3>Informasi Pribadi</h3>
										<a href="javascript:void(0)" class="panel-box-edit">UBAH</a>
									</div>
									<div class="panel-box-content">
										<ul class="list list-unstyled no-mar-bot mb-none">
											<li>Miftahur Rohman Habibie</li>
											<li>mrhabibiesmk@gmail.com</li>
											<li class="mb-lg">Berlangganan Newsletter: <a href="javascript:void(0)" data-toggle="modal" data-target="#unsubscribeNewsletter">ON</a></li>
											<li><a href="javascript:void(0)">UBAH EMAIL</a></li>
											<li><a href="javascript:void(0)">UBAH KATA SANDI</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="panel-box">
									<div class="panel-box-title">
										<h3>Alamat Pengiriman Utama</h3>
										<a href="javascript:void(0)" class="panel-box-edit">UBAH</a>
									</div>
									<div class="panel-box-content">
										<ul class="list list-unstyled no-mar-bot mb-none">
											<li>Miftahur Rohman Habibie</li>
											<li>Jl. Mer. Ruko Icon 21.</li>
											<li>Rungkut - Kota Surabaya - Jawa Timur</li>
											<li>+6282143603556</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="panel-box">
									<div class="panel-box-title">
										<h3>Alamat Penagihan Utama</h3>
										<a href="javascript:void(0)" class="panel-box-edit">UBAH</a>
									</div>
									<div class="panel-box-content">
										<ul class="list list-unstyled no-mar-bot mb-none">
											<li>Miftahur Rohman Habibie</li>
											<li>Jl. Mer. Ruko Icon 21.</li>
											<li>Rungkut - Kota Surabaya - Jawa Timur</li>
											<li>+6282143603556</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<h1 class="h2 heading-primary font-weight-normal ml-md">Pesanan Terbaru</h1>
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<td>Pesanan</td>
												<td>Dipesan pada</td>
												<td>Total</td>
												<td>Status</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><a href="javascript:void(0)">3993555772</a></td>
												<td>01/09/2017</td>
												<td>Rp. 110.000</td>
												<td>
													<ul class="list list-unstyled no-mar-bot">
														<li><span class="text-success">Telah Diterima</span></li>
														<li>Terkirim pada 04/09/2017</li>
													</ul>
												</td>
												<td><a href="javascript:void(0)">LACAK PESANAN SAYA</a></td>
												<td><a href="javascript:void(0)">PENGEMBALIAN</a></td>
											</tr>
											<tr>
												<td><a href="javascript:void(0)">366823911</a></td>
												<td>04/12/2016</td>
												<td>Rp. 34.000</td>
												<td><span class="text-muted">Selesai</span></td>
												<td>&nbsp;</td>
												<td><a href="javascript:void(0)">LACAK PESANAN SAYA</a></td>
											</tr>
											<tr>
												<td><a href="javascript:void(0)">394158145</a></td>
												<td>17/09/2016</td>
												<td>Rp. 35.000</td>
												<td><span class="text-muted">Selesai</span></td>
												<td>&nbsp;</td>
												<td><a href="javascript:void(0)">LACAK PESANAN SAYA</a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php echo $__env->make('user.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>