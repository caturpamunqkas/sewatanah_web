<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->restapi->validateKey();
		// $this->blade->sebarno('ctrl',$this);
	}

	public function index()
	{
		$this->restapi->error('no direct script access allowed!');
	}

	public function profile($url='index'){
		if($url == 'index'){
			if(!$this->input->post('token')){
				$this->restapi->error('token is required!');
				exit();
			}
			
			$token 		= $this->input->post('token');

			$where 	= array('token'=>$token);
			$auth	= UserModel::where($where)->first();

			if(!isset($auth->token)){
				$this->restapi->error('token invalid');
				exit();
			}

			$this->restapi->success($auth);

		}else if($url == 'update'){
			if(!$this->input->post('token')){
				$this->restapi->error('token is required!');
				exit();
			}

			$token 					= $this->input->post('token');
			$profile 				= UserModel::where('token',$token)->first();

			$username 				= ($this->input->post('username')) ? $this->input->post('username') : $profile->username;
			$name 					= ($this->input->post('name')) ? $this->input->post('name') : $profile->name;
			$password 				= ($this->input->post('password')) ? encrypt($this->input->post('password')) : $profile->password;
			$birthday 				= ($this->input->post('birthday')) ? $this->input->post('birthday') : $profile->birthday;
			$address 				= ($this->input->post('address')) ? $this->input->post('address') : $profile->address;
			$phone 					= ($this->input->post('phone')) ? $this->input->post('phone') : $profile->phone;

			$profile->username 		= $username;
			$profile->name 			= $name;
			$profile->password 		= $password;
			$profile->birthday 		= $birthday;
			$profile->address 		= $address;
			$profile->phone 		= $phone;
			$profile->update();

			$this->restapi->success('data has updated!');

		}else{
			$this->restapi->error('no direct script access allowed!');
		}

	}

	public function user($url='index',$token=null){
		if($url == 'index'){
			$this->restapi->error('no direct script access allowed!');
			
		}else if($url == 'register' && isset($_REQUEST['username'])){

			$user 				= new UserModel;
			$user->username 	= $this->input->post_get('username');
			$user->name 		= $this->input->post_get('name');
			$user->email 		= $this->input->post_get('email');
			
			$cekmail 				= UserModel::where('email',$this->input->post_get('email'))->first();
			
			if($cekmail){
				$this->restapi->error('email alredy registered',500);
				exit();
			}

			$cekuser 				= UserModel::where('username',$this->input->post_get('username'))->first();
			
			if($cekuser){
				$this->restapi->error('username alredy registered',500);
				exit();
			}


			$user->password 	= encrypt($this->input->post_get('password'));
			$user->absent		= $this->input->post_get('absent');
			$user->birthday 	= $this->input->post_get('birthday');
			$user->address 		= $this->input->post_get('address');
			$user->phone		= $this->input->post_get('phone');
			$user->majors 		= $this->input->post_get('majors');

			try {
				$user->save();
				$this->restapi->success('data saved!');
			}catch(Exception $e) {
				if(strpos($e->getMessage(), 'Duplicate entry') > 1){
					if(strrpos($e->getMessage(), "for key 'absent'") > 1){
						$this->restapi->error('absent alredy registered',500);
					}
				}else{
					$this->restapi->error($e->getMessage(),500);
				}
			}

		}else if($url == 'login'){
			$user 		= $this->input->post('username');
			$pass		= $this->input->post('password');

			if(strpos($user, '@') > 1){
				$where 	= array(
						'email' 	=> $user,
					  );
			}else{
				$where 	= array(
						'username' 	=> $user,
					  );
			}

			$auth	= UserModel::where($where)->first();

			if(!isset($auth->id)){
				$this->restapi->error('incorect user');
				exit();
			}else if($auth->token == null){
			    $auth->token = getToken(10);
 				$auth->save();
			}
// 			else{
// 				$auth->token = getToken(10);
// 				$auth->save();
// 			}

			if($auth->password !== encrypt($pass)){
				$this->restapi->error('incorect password');
				exit();
			}
			
			$get = array('token'=>$auth->token,'imagedir'=>$auth->imagedir,'majors'=>$auth->majors,'email'=>$auth->email,'name'=>$auth->name,'user'=>$auth);

			$this->restapi->success($get);

		}else if($url == 'data'){

			$token 		= $this->input->post('token');
			$where 	= array(
					'token' 	=> $token
				  );

			$auth	= UserModel::where($where)->first();

			if(!isset($auth->token)){
				$this->restapi->error('token invalid');
				exit();
			}

			$this->restapi->success($auth);

		}else if($url == 'tabungan'){

			$token 		= $this->input->post('token');
			$where 	= array(
					'token' 	=> $token
				  );

			$auth	= UserModel::where($where)->with(['tabungan' => function($query) {
						        $query->orderBy('created_at', 'DESC');
						    }])->first();
			
			if(!isset($auth->token)){
				$this->restapi->error('token invalid');
				exit();
			}
			
			$total    = number_format($auth->tabungan->sum("value"),0,',','.');
			foreach($auth->tabungan as $result){
			    $result->text = number_format($result->value,0,',','.');
			}
			$resp   = [
			            "total"=>[
			                "value"=>$auth->tabungan->sum("value"),
			                "text"=>$total
			                ],
			            "tabungan"=>$auth->tabungan
			            ];

			$this->restapi->success($resp);
			
		}else if($url == 'submitVoucher'){
		    
		    $token 		= $this->input->post('token');
		    $vcode   	= $this->input->post('voucher');
		    
		    #cek code voucher
		    $voucher    = VoucherModel::where("token",$vcode)->first();
		    
		    if(!isset($voucher->id)){
		        $this->restapi->error('Voucher invalid!');
				exit();
		    }else if($voucher->uses == "Y" && $voucher->id_user !== null){
		        $this->restapi->error('Voucher has been used!');
				exit();
		    }
		    
		    $user   = UserModel::where("token",$token)->first();
		    if(!isset($user->id)){
				$this->restapi->error('token invalid');
				exit();
			}
			
			$add    			= new TabunganModel;
			$add->id_user 		= $user->id;
			$add->id_voucher 	= $voucher->id;
			$add->value 		= $voucher->nominal;
		    $add->save();
			$voucher->id_user 	= $user->id;
			$voucher->uses    	= "Y";
			$voucher->update();

			$where 	= array(
					'token' 	=> $token
				  );

			$auth	= UserModel::where($where)->with(['tabungan' => function($query) {
						        $query->orderBy('created_at', 'DESC');
						    }])->first();
			
			$resp   = [
			            "message"=>"Success add Voucher!",
			            "total"=>[
			            	"value"=>$auth->tabungan->sum("value"),
			            	"text"=>number_format($auth->tabungan->sum("value"),0,',','.')
			            ],
			            "value"=>[
			                        "value"=>$voucher->nominal,
			                        "text"=>number_format($voucher->nominal,0,',','.')
			                        ]
			            ];
			$this->restapi->success($resp);
		    
		}else{
			$this->restapi->error('no direct script access allowed!');
			exit();
		}
	}

	public function pengumuman(){
		$auth	= PengumumanModel::orderBy("started")->get();
		$exp    = strtotime(date("Y-m-d H:i:s"));
		foreach($auth as $result){
            $tgl = $result->started;
		    $result->tanggal = tgl_indo($tgl);
		    $expi = strtotime($result->expired);
		    if($expi < $exp){
		        PengumumanModel::find($result->id)->delete();
		        $auth	= PengumumanModel::desc()->get();
		    }
		}
		$this->restapi->success($auth);
	}

	public function jadwal(){
	    $senin = $selasa = $rabu = $kamis = $jumat = [];
		$auth	= JadwalModel::asc()->get();
		foreach($auth as $result){
		    $senin[]    = $result->senin;
		    $selasa[]   = $result->selasa;
		    $rabu[]     = $result->rabu;
		    $kamis[]    = $result->kamis;
		    $jumat[]    = $result->jumat;
		}
		$json = [
		    "senin"     => $senin,
		    "selasa"    => $selasa,
		    "rabu"      => $rabu,
		    "kamis"     => $kamis,
		    "jumat"     => $jumat
		    ];
		$this->restapi->success($json);
	}

	public function piket($rule="sekarang"){
		if($rule == "senin"){
			$json = PiketModel::where("hari","senin")->with("user")->asc()->get();
		}else if($rule == "selasa"){
			$json = PiketModel::where("hari","selasa")->with("user")->asc()->get();
		}else if($rule == "rabu"){
			$json = PiketModel::where("hari","rabu")->with("user")->asc()->get();
		}else if($rule == "kamis"){
			$json = PiketModel::where("hari","kamis")->with("user")->asc()->get();
		}else if($rule == "jumat"){
			$json = PiketModel::where("hari","jumat")->with("user")->asc()->get();
		}else if($rule == "sekarang"){
			$json = PiketModel::where("hari",hari_now())->with("user")->asc()->get();
		}else{
			$json = PiketModel::desc()->with("user")->get();
		}
		$this->restapi->success($json);
	}
	
	public function getToken() {
	    $voucher = strtoupper(getToken(4)."-".getToken(4)."-".getToken(4)."-".getToken(4)."-".getToken(4));
	    $this->restapi->success($voucher);
	}

	public function encrypt($string){
		echo encrypt($string);
	}


}