<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->restapi->validateKey();
		// $this->blade->sebarno('ctrl',$this);
	}

	public function index()
	{
		$this->restapi->error('no direct script access allowed!');
	}

	public function profile($url='index'){
		if($url == 'index'){
			if(!$this->input->post('id_user')){
				$this->restapi->error('token is required!');
				exit();
			}
			
			$IDuser 		= $this->input->post('id_user');

			$where 	= array('id_user'=>$IDuser);
			$auth	= UserModel::where($where)->first();

			$listlahan = LahanModel::where('id_user', $IDuser)->desc()->get();
			$lahanNotVerif = LahanModel::notVerif()->where('id_user', $IDuser)->desc()->get();

			$lahan = array(
				'list' => $listlahan, 
				'total' => count($listlahan), 
				'notverif' => count($lahanNotVerif), 
				'terverif' => count($listlahan)-count($lahanNotVerif));


			// if(!isset($auth->id_user)){
			// 	$this->restapi->error('token invalid');
			// 	exit();
			// }

			$data = array('user' => $auth, 'lahan' => $lahan );

			$this->restapi->success($data);

		}else if($url == 'update'){
			if(!$this->input->post('token')){
				$this->restapi->error('token is required!');
				exit();
			}

			$token 					= $this->input->post('token');
			$profile 				= UserModel::where('token',$token)->first();

			$username 				= ($this->input->post('username')) ? $this->input->post('username') : $profile->username;
			$name 					= ($this->input->post('name')) ? $this->input->post('name') : $profile->name;
			$password 				= ($this->input->post('password')) ? encrypt($this->input->post('password')) : $profile->password;
			$birthday 				= ($this->input->post('birthday')) ? $this->input->post('birthday') : $profile->birthday;
			$address 				= ($this->input->post('address')) ? $this->input->post('address') : $profile->address;
			$phone 					= ($this->input->post('phone')) ? $this->input->post('phone') : $profile->phone;

			$profile->username 		= $username;
			$profile->name 			= $name;
			$profile->password 		= $password;
			$profile->birthday 		= $birthday;
			$profile->address 		= $address;
			$profile->phone 		= $phone;
			$profile->update();

			$this->restapi->success('data has updated!');

		}else{
			$this->restapi->error('no direct script access allowed!');
		}
	}

	public function user($url='index',$token=null){
		if($url == 'index'){
			$this->restapi->error('no direct script access allowed!');
			
		}else if($url == 'register' && isset($_REQUEST['username'])){

			$user 				= new UserModel;
			$user->username 	= $this->input->post_get('username');
			$user->name 		= $this->input->post_get('name');
			$user->email 		= $this->input->post_get('email');
			
			$cekmail 				= UserModel::where('email',$this->input->post_get('email'))->first();
			
			if($cekmail){
				$this->restapi->error('email alredy registered',500);
				exit();
			}

			$cekuser 				= UserModel::where('username',$this->input->post_get('username'))->first();
			
			if($cekuser){
				$this->restapi->error('username alredy registered',500);
				exit();
			}


			$user->password 	= encrypt($this->input->post_get('password'));
			$user->absent		= $this->input->post_get('absent');
			$user->birthday 	= $this->input->post_get('birthday');
			$user->address 		= $this->input->post_get('address');
			$user->phone		= $this->input->post_get('phone');
			$user->majors 		= $this->input->post_get('majors');

			try {
				$user->save();
				$this->restapi->success('data saved!');
			}catch(Exception $e) {
				if(strpos($e->getMessage(), 'Duplicate entry') > 1){
					if(strrpos($e->getMessage(), "for key 'absent'") > 1){
						$this->restapi->error('absent alredy registered',500);
					}
				}else{
					$this->restapi->error($e->getMessage(),500);
				}
			}

		}else if($url == 'login'){
			$user 		= $this->input->post('username');
			$pass		= $this->input->post('password');

			$where 	= array('email' 	=> $user,);

			$auth	= UserModel::where($where)->first();

			if(!isset($auth->id_user)){
				$this->restapi->error('User Tidak Ditemukan');
				exit();
			}

			if($auth->password !== md5($pass)){
				$this->restapi->error('Mohon Maaf Password Salah');
				exit();
			}
			
			$get = array('user'=>$auth);

			$this->restapi->success($auth);

		}else{
			$this->restapi->error('no direct script access allowed!');
			exit();
		}
	}

	public function dashboard(){
		$get5 = LahanModel::desc()->take(5)->get();
		$getAll = LahanModel::desc()->get();

		// $array = array('get5' => $get5, 'getAll' => $getAll);

		$this->restapi->success($getAll);
	}

	public function kategori()
	{
		$kat = KategoriModel::desc()->get();

		$this->restapi->success($kat);
	}
	
	public function getToken() {
	    $voucher = strtoupper(getToken(4)."-".getToken(4)."-".getToken(4)."-".getToken(4)."-".getToken(4));
	    $this->restapi->success($voucher);
	}

	public function encrypt($string){
		echo encrypt($string);
	}


}