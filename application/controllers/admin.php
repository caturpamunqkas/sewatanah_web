<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "adminLogin"){
			redirect(base_url("auth/admin"));
		}
		$this->data['config'] 				= ConfigModel::find(1);

		$this->blade->share('ctrl', $this);
	}

	public function index() {
		$data = $this->data;
		$data['title'] = "Sewa Tanah";
		$data['menu'] = "dashboard";

		$data['user_all'] = UserModel::desc()->get();
		$data['user'] = UserModel::notVerif()->desc()->get();
		$data['lahan_all'] = LahanModel::desc()->get();
		$data['lahan'] = LahanModel::notVerif()->desc()->get();

		echo $this->blade->tampil('admin.dashboard.index', $data);
	}

	public function user($url=null, $idUser=null){
		$data 						= $this->data;
		switch ($url) {
			case 'view':
				$data['title'] = "SEWATANAH | List User";
				$data['menu'] = "listuser";

				$data['data'] = UserModel::find($idUser);
				$data['lahan'] = LahanModel::where('id_user',$data['data']->id_user)->get();

				if(!isset($data['data']->id)){
					redirect('/admin/user');
					return;
				}

				// echo toJson($data);
				echo $this->blade->tampil('admin.listuser.view', $data);	
				break;

			case 'verif':
				$user = UserModel::find($idUser);
				if (!isset($user->id)) {
					redirect('/admin/user/view');
				}

				$data = $this->input->post('verif');
				$user->status = $data;
				$user->save();
				break;

			case 'alert':
				$user = UserModel::find($idUser);

				if (!isset($user->id)) {
					redirect('/admin/user/view');
				}

				$user->alert = $user->alert + 1;
				$user->save();

				if ($user->alert > 2) {
					$freeze = new FreezeModel;
					$freeze->id_user = $user->id_user;
					$freeze->startOn = date('Y-m-d');
					$freeze->endOn = date('Y-m-d', strtotime('+7 day'));
					if ($user->alert >= 3) {
						$freeze->save();
						$user->status = 'freeze';
						$user->save();
					}
				}
				elseif ($user->alert >= 5) {
					$user->status = 'blocked';
					$user->save();
					
					LahanModel::where('id_user', $user->id_user)->update(['status'=>'block']);
					FreezeModel::where('id_user', $user->id_user)->delete();
				}

				break;

			case 'delete':
				$ID_USER = UserModel::find($idUser);
				if(!isset($ID_USER->id)){
					redirect('/admin/user');
					return;
				}
				LahanModel::where('id_user',$ID_USER->id_user)->delete();
				$ID_USER->delete();

				redirect('/admin/user');
				break;

			case 'pulihkan':
				UserModel::where('id_user', $idUser)->update(['status'=>'null']);
				LahanModel::where('id_user', $idUser)->update(['status'=>'false']);
				break;
			
			default:
				$data['title'] = "SEWATANAH | List User";
				$data['menu'] = "listuser";

				// $data = $this->mymodel->GetUserList();
				$data['data'] = UserModel::desc()->get();

				echo $this->blade->tampil('admin.listuser.index',$data);
				break;
		}
	}

	public function lahan($value=null, $idLahan=null, $idUser=null) {
		$data 						= $this->data;
		switch ($value) {
			case 'view':
				$data['title'] = "SEWATANAH | Lihat Lahan";
				$data['menu'] = "listlahan";

				$data['data'] = LahanModel::with('user')->find($idLahan);

				if(!isset($data['data']->id)){
					redirect('/admin/lahan');
					return;
				}

				// echo toJson($data);
				echo $this->blade->tampil('admin.listlahan.view', $data);
				break;
			case 'delete':
				$ID_LAHAN = LahanModel::find($idLahan);
				$ID_LAHAN->delete();

				redirect('/admin/lahan');
				break;	
			case 'ubah':

				$LAHAN = LahanModel::find($idLahan);
				$USER = UserModel::find($idUser);

				if (!isset($LAHAN->id)) {
					redirect('/admin/lahan');
				}

				$alert	=	$this->input->post('alert'); 
				$delete	=	$this->input->post('delete');
				$cancel	=	$this->input->post('cancel');
				$verif	=	$this->input->post('verif');

				if ($alert != null) {
					$USER->alert = $USER->alert + 1;
					$USER->save();

					$LAHAN->status = 'false';
					$LAHAN->save();

					$cari = FreezeModel::where('id_user', $USER->id_user)->first();

					if ($USER->alert >= 3 && !$cari) {
						$freeze = new FreezeModel;
						$freeze->id_user = $USER->id_user;
						$freeze->startOn = date('Y-m-d');
						$freeze->endOn = date('Y-m-d', strtotime('+7 day'));
						
						$freeze->save();
						$USER->status = 'freeze';
						$USER->save();
						// if ($cari == null) {
						// 	$freeze->save();
						// 	$USER->status = 'freeze';
						// 	$USER->save();
						// }
					}
					elseif ($USER->alert >= 5) {
						$USER->status = 'blocked';
						$USER->save();
						
						LahanModel::where('id_user', $USER->id_user)->update(['status'=>'block']);
						FreezeModel::where('id_user', $USER->id_user)->delete();
					}
					
				}
				elseif ($verif != null) {
					$LAHAN->status = 'true';
					$LAHAN->save();
				}elseif ($cancel != null) {
					$LAHAN->status = 'false';
					$LAHAN->save();
				}elseif ($delete != null) {
					redirect(base_url()."admin/lahan/delete/".$idLahan);
				}

				break;	
			default:
				$data['title'] = "SEWATANAH | List Lahan";
				$data['menu'] = "listlahan";

				$data['data'] = LahanModel::with('user','kategori')->desc()->get();
				// $data['kategori'] = KategoriModel::desc()->get();

				// echo toJson($data);
				echo $this->blade->tampil('admin.listlahan.index', $data);
				break;
		}
	}

	public function feedback() {
		$data 						= $this->data;
		$data['title'] = "SEWATANAH | Feedback";
		$data['menu'] = "feedback";

		echo $this->blade->tampil('admin.feedback.index', $data);
	}

	public function test(){
		$LAHAN = LahanModel::find(3);
		echo toJson($LAHAN);
	}

	public function config($page=null){
		$data 						= $this->data;
		switch ($page) {
			case 'slider':
				$data['title'] = "SEWATANAH | Slider Website";
				$data['menu'] = "config";
				$data['slider'] = SliderModel::desc()->get();

				echo $this->blade->tampil('admin.config.slider', $data);
				// echo $this->blade->tampil('admin.slider.index', $data);
				break;

			case 'website':
				$data['title'] = "SEWATANAH | Config Website";
				$data['menu'] = "config";

				echo $this->blade->tampil('admin.config.index', $data);
				break;

			case 'websiteSave':
				if ($this->input->is_ajax_request() == true) {
					$config 			= $data['config'];
					$config->nama 		= $this->input->post('nama');
					$config->deskripsi	= $this->input->post('deskripsi');
					$config->email 		= $this->input->post('email');

					$config->save();
				}
				break;
			
			default:
				$data['title'] = "SEWATANAH | Config Website";
				$data['menu'] = "config";

				echo $this->blade->tampil('admin.config.index', $data);
				break;
		}
	}

	// PRIVATE SECTION ---------------------------------------------
	private function upload_files($path,$files,$filename=false)
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'max_size'		=> '2000',
            'overwrite'     => false,
        );

        if($filename){
        	$config['file_name'] 	= $filename;
        }else {
        	$config['encrypt_name'] 	= FALSE;
        }

        $this->load->library('upload', $config);

        $images 		= array();
        $data['msg']	= array();
        $data['auth']	= false;
        foreach ($files['name'] as $key => $image) {
            $_FILES['image[]']['name']		= $files['name'][$key];
            $_FILES['image[]']['type']		= $files['type'][$key];
            $_FILES['image[]']['tmp_name']	= $files['tmp_name'][$key];
            $_FILES['image[]']['error']		= $files['error'][$key];
            $_FILES['image[]']['size']		= $files['size'][$key];

            $this->upload->initialize($config);

            if ($this->upload->do_upload('image[]')) {
            	$data['auth']		= true;
            	array_push($data['msg'],$this->upload->data());
            } else {
            	$data['auth']		= ($data['auth']==true) ? true : false;
            	array_push($data['msg'],$this->upload->display_errors());
            }
        }

        return $data;
    }

    
    private function upload($dir,$name ='userfile',$filename=false){
		$config['upload_path']      = $dir;
        $config['allowed_types']    = 'gif|jpg|png|jpeg';
        $config['max_size']         = 2000;

        if($filename){
        	$config['file_name'] 	= $filename;
        }else {
        	$config['encrypt_name'] 	= FALSE;
        }
        
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload($name))
        {		
        		$data['auth'] 	= false;
                $data['msg'] 	= $this->upload->display_errors();
                return $data;
        }
        else
        {
        		$data['auth']	= true;
        		$data['msg']	= $this->upload->data();
        		return $data;
        }
	}

	private function upload_materi($type='file',$dir,$name ='userfile',$filename=null){
		$config['upload_path']      = $dir;
		$config['allowed_types'] 	= "*";

		if($type=='document'){
			$config['allowed_types']    = 'pdf|doc|docx|ppt|pptx|xls|xlsx|txt|text|jpeg|jpg|png';
		}
		else if ($type=='video'){
			$config['allowed_types']    = 'mkv|mp4|avi|flv|mov';
		}
		else if($type=="audio"){
			//$config['allowed_types']    = 'm4a|mp3|M4A|ogg|wav|mp4|x-m4a';	
			
		}
		else {
			$config['allowed_types']    = 'iso|7z|rar|zip';
		}
        

        if($filename==null){
        	$config['encrypt_name'] 	= FALSE;
        }
        else {
        	$config['file_name'] 		= $filename;
        }
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($name))
        {		
        		$data['auth'] 	= false;
                $data['msg'] 	=$this->upload->display_errors();
                return $data;
        }
        else
        {
        		$data['auth']	= true;
        		$data['msg']	= $this->upload->data();
        		return $data;
        }
	}

	private function uploadVideo($dir,$name,$filename=null){
		$config['upload_path']      = $dir;
		$config['allowed_types']    = 'mkv|mp4|avi|flv|mov';

        if($filename==null){
        	$config['encrypt_name'] 	= FALSE;
        }
        else {
        	$config['file_name'] 		= $filename;
        }
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($name))
        {		
        		$data['auth'] 	= false;
                $data['msg'] 	=$this->upload->display_errors();
                return $data;
        }
        else
        {
        		$data['auth']	= true;
        		$data['msg']	= $this->upload->data();
        		return $data;
        }
	}

	private function isVideo($file){
		$mime 		= ['video/mp4','video/x-matroska','video/x-msvideo','video/quicktime','video/x-flv'];
		if ( in_array($_FILES[$file]['type'], $mime)){
			return true;
		}
		else {
			return false;
		}
	}

	private function isAudio($file){
		$mime 		= ['audio/mpeg','audio/mp3','audio/x-m4a','audio/mp4','audio/x-aiff','audio/ogg','audio/vnd.wav'];
		if ( in_array($_FILES[$file]['type'], $mime)){
			return true;
		}
		else {
			return false;
		}
	}

	private function isImage($file){
		if ((($_FILES[$file]['type'] == 'image/gif') || ($_FILES[$file]['type'] == 'image/jpeg') || ($_FILES[$file]['type'] == 'image/png'))){
			return true;
		}
		else {
			return false;
		}
	}

	private function isDocument($file){
		if ( $_FILES[$file]['type'] == 'application/pdf' || $_FILES[$file]['type'] == 'application/msword' 
			|| $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' 
			|| $_FILES[$file]['type'] == 'application/vnd.ms-excel'  
			|| $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' 
			|| $_FILES[$file]['type'] == 'application/vnd.ms-powerpoint' 
			|| $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.presentationml.presentation' 
			|| $_FILES[$file]['type'] == 'text/plain'  
			|| $_FILES[$file]['type'] == 'image/gif'
			|| $_FILES[$file]['type'] == 'image/jpeg' 
			|| $_FILES[$file]['type'] == 'image/png' 
		){

			return true;
		}
		else {
			return false;
		}
	}

	private function isArchive($file){
		if ( $_FILES[$file]['type'] == 'application/x-compressed' 
			|| $_FILES[$file]['type'] == 'application/x-zip-compressed' 
			|| $_FILES[$file]['type'] == 'application/zip' 
			|| $_FILES[$file]['type'] == 'multipart/x-zip' 
			|| $_FILES[$file]['type'] == 'application/x-rar-compressed' 
			|| $_FILES[$file]['type'] == 'application/octet-stream' 
			|| $_FILES[$file]['type'] == 'application/x-7z-compressed'  || $_FILES[$file]['type'] == 'application/x-gtar'   ){
			return true;
		}
		else {
			return false;
		}
	}
	// END PRIVATE SECTIOn
}