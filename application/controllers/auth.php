<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->data['config']               = ConfigModel::find(1);
        $this->data['session']              = $this->session->userdata('status');
        $where = array('email' => $this->session->userdata('nama'));
        $this->data['user']                 = UserModel::where($where)->first();

        $this->blade->share('ctrl', $this);
    }

	public function index(){
        $data                       = $this->data;
        if($this->session->userdata('status') == "adminLogin"){
			redirect(base_url("admin"));
		}

        if($this->session->userdata('status') == "userLogin"){
            redirect(base_url("user"));
        }

        redirect('auth/user');
    }

    public function user($url='login'){
        $data                       = $this->data;
        $data['session']    = $this->session->userdata('status');

        if ($data['session'] == 'userLogin') {
            redirect('user');
        }

        if ($url=='register') {
            echo $this->blade->tampil('user.register.index', $data);
        }elseif ($url=='login'){
            echo $this->blade->tampil('user.login.index', $data);
        }
    }

    public function admin(){
        $data                       = $this->data;

        if ($data['session'] == 'adminLogin') {
            redirect('admin');
        }

        echo $this->blade->tampil('admin.login.index');
    }
    
    public function login($url=null){
        $data                       = $this->data;
        switch ($url) {
            case 'admin':
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $where = array(
                    'username' => $username
                    );

                $auth   = AdminModel::where($where)->first();


                if(!isset($auth->id)){
                    echo "Opps! Username Tidak Di Temukan";
                    return;
                }

                if ($auth->password !== md5($password)) {
                    echo "Password Salah";
                    return;
                }else{
                    $data_session = array(
                        'nama' => $email,
                        'status' => "adminLogin"
                        );

                    $this->session->set_userdata($data_session);

                    redirect(base_url("admin"));
                }
            break;

            case 'user':
                $email =  $this->input->post('email');
                $password = $this->input->post('password');

                $where  = array(
                    'email' => $email,
                  );

                $auth   = UserModel::where($where)->first();


                if(!isset($auth->id)){
                    echo "Opps! Username Tidak Di Temukan";
                    return;
                }

                if ($auth->password !== md5($password)) {
                    echo "Password Salah";
                    return;
                }else{
                    $data_session = array(
                        'nama' => $email,
                        'status' => "userLogin"
                        );

                    $this->session->set_userdata($data_session);

                    redirect(base_url("user"));
                }
                break;

            default:
                # code...
                break;
        }
    }

    public function out($url=null){
		switch ($url) {
            case 'admin':
                $this->session->sess_destroy();
                redirect(base_url('auth/admin'));
                break;

            case 'user':
                $this->session->sess_destroy();
                redirect(base_url('auth/user'));
                break;
            
            default:
                # code...
                break;
        }
	}
}