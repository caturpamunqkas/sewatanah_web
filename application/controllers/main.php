<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	public function __construct(){
		parent::__construct();
		
		$this->data['config'] 				= ConfigModel::find(1);
		$this->data['session']				= $this->session->userdata('status');
		$where = array('email' => $this->session->userdata('nama'));
		$this->data['user']					= UserModel::where($where)->first();

		$this->blade->share('ctrl', $this);
	}

	public function index(){
		$data 						= $this->data;
		
		$data['title'] = $data['config']->nama;
		$data['slider'] = SliderModel::notDraft()->desc()->get();

		echo $this->blade->tampil('main.home.index', $data);
	}

}