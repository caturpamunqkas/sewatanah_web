<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "userLogin"){
			redirect(base_url("auth/user"));
		}
		$this->data['config'] 				= ConfigModel::find(1);
		$where = array('email' => $this->session->userdata('nama'));
		$this->data['user']					= UserModel::where($where)->first();
		$this->data['session']	= $this->session->userdata('status');

		$this->blade->share('ctrl', $this);
	}

	public function index(){
		$data = $this->data;
		$data['menu'] = "index";
		echo $this->blade->tampil('user.dashboard.index', $data);
	}

	public function profile($link=null){
		$data = $this->data;
		$data['menu'] = "profile";

		$data['data'] = UserModel::find($data['user']->id);

		if ($link == 'password') {
			if ($this->input->post('password_baru') != $this->input->post('confirm_password')) {
				return 0;
			}

			
		}

		// echo toJson($data['data']);
		echo $this->blade->tampil('user.profile.index', $data);
	}

	public function lahan($value=null, $idLahan=null, $idUser=null) {
		$data 						= $this->data;
		switch ($value) {
			case 'view':
				$data['title'] = "SEWATANAH | Lihat Lahan";
				$data['menu'] = "listlahan";

				$data['data'] = LahanModel::with('user')->find($idLahan);

				if(!isset($data['data']->id)){
					redirect('/admin/lahan');
					return;
				}

				// echo toJson($data);
				echo $this->blade->tampil('admin.listlahan.view', $data);
				break;
			case 'delete':
				$ID_LAHAN = LahanModel::find($idLahan);
				$ID_LAHAN->delete();

				redirect('/admin/lahan');
				break;	
			case 'ubah':

				$LAHAN = LahanModel::find($idLahan);
				$USER = UserModel::find($idUser);

				if (!isset($LAHAN->id)) {
					redirect('/admin/lahan');
				}

				$alert	=	$this->input->post('alert'); 
				$delete	=	$this->input->post('delete');
				$cancel	=	$this->input->post('cancel');
				$verif	=	$this->input->post('verif');

				if ($alert != null) {
					$USER->alert = $USER->alert + 1;
					$USER->save();

					$LAHAN->status = 'false';
					$LAHAN->save();

					if ($USER->alert >= 5) {
					$USER->status = 'blocked';
					$USER->save();
					FreezeModel::where('id_user', $USER->id_user)->delete();
					} elseif ($USER->alert >= 3) {
						$freeze = new FreezeModel;
						$freeze->id_user = $USER->id_user;
						$freeze->startOn = date('Y-m-d');
						$freeze->endOn = date('Y-m-d', strtotime('+7 day'));
						if ($USER->alert < 3) {
							$freeze->save();
						}

						$USER->status = 'freeze';
						$USER->save();
					}
				}
				elseif ($verif != null) {
					$LAHAN->status = 'true';
					$LAHAN->save();
				}

				break;	
			default:
				$data['title'] = "List Lahan Anda";
				$data['menu'] = "listlahan";

				$data['data'] = LahanModel::with('kategori')->where('id_user', $data['user']->id_user)->desc()->get();
				// $data['kategori'] = KategoriModel::desc()->get();

				// echo toJson($data);
				echo $this->blade->tampil('user.lahan.index', $data);
				break;
		}
	}

}