<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function toObject($var){
    return json_decode(json_encode($var));
}

if(!function_exists("utf8ize")){
	function utf8ize($d) {
	    if (is_array($d)) {
	        foreach ($d as $k => $v) {
	            $d[$k] = utf8ize($v);
	        }
	    } else if (is_string ($d)) {
	        return utf8_encode($d);
	    }
	    return $d;
	}
}

if (! function_exists('goResult')){

	function goResult($def,$msg){
    	$data['auth'] 	= $def;
			$data['msg'] 	= $msg;
			return toJson($data);
    }
}

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function dateResource($type){
	switch ($type) {
		case 'day':
			$data 	= [
							[
								'key' 	=> '1',
								'value'	=> 'Senin',
							],
							[
								'key' 	=> '2',
								'value'	=> 'Selasa',
							],
							[
								'key' 	=> '3',
								'value'	=> 'Rabu',
							],
							[
								'key' 	=> '4',
								'value'	=> 'Kamis',
							],
							[
								'key' 	=> '5',
								'value'	=> 'Jumat',
							],
							[
								'key' 	=> '6',
								'value'	=> 'Sabtu',
							],
							[
								'key' 	=> '7',
								'value'	=> 'Minggu',
							],
					   ];
			return json_decode(json_encode($data));
			break;
		case 'month':
			$data 	= [
							[
								'key' 	=> '1',
								'value'	=> 'Januari',
							],
							[
								'key' 	=> '2',
								'value'	=> 'Februari',
							],
							[
								'key' 	=> '3',
								'value'	=> 'Maret',
							],
							[
								'key' 	=> '4',
								'value'	=> 'April',
							],
							[
								'key' 	=> '5',
								'value'	=> 'Mei',
							],
							[
								'key' 	=> '6',
								'value'	=> 'Juni',
							],
							[
								'key' 	=> '7',
								'value'	=> 'July',
							],
							[
								'key' 	=> '8',
								'value'	=> 'Augustus',
							],
							[
								'key' 	=> '9',
								'value'	=> 'September',
							],
							[
								'key' 	=> '10',
								'value'	=> 'Oktober',
							],
							[
								'key' 	=> '11',
								'value'	=> 'November',
							],
							[
								'key' 	=> '12',
								'value'	=> 'Desember',
							],
					   ];
			return json_decode(json_encode($data));
			break;
		default:
			return false;
			break;
	}
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
    }

    return $token;
}

function getSubdomain(){
	return array_shift((explode(".",$_SERVER['HTTP_HOST'])));
}

function urlRedirect($url){
	echo '<script>window.location.href="'.$url.'"</script>';
	return;
}


if ( ! function_exists('read_more'))
{
	/**
	 * Site URL
	 *
	 * Create a local URL based on your basepath. Segments can be passed via the
	 * first parameter either as a string or an array.
	 *
	 * @param	string	$uri
	 * @param	string	$protocol
	 * @return	string
	 */
	function read_more($string,$limit=100)
	{
		$string = trim(preg_replace('/\s+/', ' ', $string));
		$string = trim(preg_replace('/\t+/', '', $string));
		$string = str_replace('&nbsp;','',$string);
		$length = strlen(strip_tags($string));
		if ($length>$limit){
			return substr(strip_tags($string),0,$limit).' ... ';
		}
		else {
			return strip_tags($string);
		}
	}
}



if ( ! function_exists('find_replace'))
{
	/**
	 * Site URL
	 *
	 * Create a local URL based on your basepath. Segments can be passed via the
	 * first parameter either as a string or an array.
	 *
	 * @param	string	$uri
	 * @param	string	$protocol
	 * @return	string
	 */
	function find_replace($string,$find)
	{
		$bodytag = str_replace($find, "<b><u>".$find."</u></b>", strtolower($string));
		return $bodytag;
	}
}

if ( ! function_exists('toJson'))
{
	function toJson($var) {
	    header('Content-Type: application/json');
	    return json_encode($var);
	}
}


if ( ! function_exists('goExplode'))
{
	function goExplode($string,$delimiter="-",$result=0) {
	    $var 	= explode($delimiter, $string);
	    if($result==0){
	    	return $var[0];
	    }

	    if(!isset($var[$result])){
	    	return;
	    }

	    return $var[$result];
	}
}



if ( ! function_exists('seo'))
{
	function seo($s) {
	    $c = array (' ');
	    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
	    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
	    $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
	    return $s;
		}
}

if ( ! function_exists('match'))
{
	function match($val,$val2,$return,$return_2=null){

		if($return_2==null){
			if ($val==$val2){
				return $return;
			}	
		}
		else {
			if ($val==$val2){
				return $return;
			}else{
				return $return_2;
			}		
		}
		
	}
}

if ( ! function_exists('daysLeft'))
{
	function daysLeft($date,$day){
		date_default_timezone_set('Asia/Jakarta');
		$tanggal = Date('Y-m-d');
		$now 	= new DateTime($tanggal);
		$date 	= new DateTime($date);
		$result = $now->diff($date);

		return (($day - $result->days)<=0) ? 0 : $day - $result->days ;
	}
}

if ( ! function_exists('dateLeft') ) {

	function dateLeft($date){
		$tanggal 	= Date('Y-m-d');
		$date1 		= new DateTime($tanggal);
		$date2 		= new DateTime($date);
		if($date2<$date1){
			return 0;
		}
		$diff 		= $date1->diff($date2);
		return $diff->days;
	}
}


if ( ! function_exists('zero'))
{
	function zero($data,$url){
		if(count($data)<=0){
			redirect($url);
			exit;
		}
	}
}


if ( ! function_exists('remFile'))
{
	function remFile($path){
		if(file_exists($path)){
			if(unlink($path)){
				return true;	
			}
			return false;
		}

		return false;
	}
}

if (!function_exists('img_holder')){
	function img_holder($type=null){
		switch ($type) {
			case 'food':
				return base_url().'/assets/images/placeholder/food.png';
				break;
			case 'profile':
			    return base_url().'/assets/images/placeholder/avatar.png';
				break;
			case 'women':
			    return base_url().'/assets/images/placeholder/women.png';
			break;
			case 'long-ads':
			    return base_url().'/assets/images/placeholder/long-ads.png';
			break;
			case 'box-ads':
			    return base_url().'/assets/images/placeholder/box-ads.png';
			break;
			case 'camera':
			    return base_url().'/assets/images/placeholder/camera.jpg';
			break;
			case 'video':
			    return base_url().'/assets/images/placeholder/video.jpg';
			break;
			case 'flat':
			    return base_url().'/assets/images/placeholder/flat.jpg';
			break;
			case 'folder':
			    return base_url().'/assets/images/placeholder/folder.png';
			break;
			default:
				return base_url().'/assets/images/placeholder/basic.png';
				# code...
				break;
		}
	}
}


if (!function_exists('notif_type')){
	function notif_type($data,$type=null){

		switch ($data) {
			case 'restaurant':
				$hasil 		= ($type==null) ? '<span class="label label-danger  label-icon"><i class="fa fa-cutlery"></i></span>' : '<span class="label label-danger  label-icon"><i class="fa fa-cutlery"></i></span>';
				break;
			case 'users':
			    $hasil 		= ($type==null) ? '<span class="label label-primary  label-icon"><i class="fa fa-users"></i></span>' : '<span class="label label-primary  label-icon"><i class="fa fa-users"></i></span>';
				break;
			case 'preorder':
			    $hasil 		= ($type==null) ? '<span class="label label-success  label-icon"><i class="fa fa-truck"></i></span>' : '<span class="label label-success  label-icon"><i class="fa fa-truck"></i></span>';
				break;
			case 'promo':
			    $hasil 		= ($type==null) ? '<span class="label label-warning  label-icon"><i class="fa fa-gift"></i></span>' : '<span class="label label-warning  label-icon"><i class="fa fa-gift"></i></span>';
				break;
			case 'review':
			    $hasil 		= ($type==null) ? '<span class="label label-info  label-icon"><i class="fa fa-newspaper-o"></i></span>' : '<span class="label label-info  label-icon"><i class="fa fa-newspaper-o"></i></span>';
				break;
			case 'message':
			    $hasil 		= ($type==null) ? '<span class="label bg-purple  label-icon"><i class="fa fa-envelope-o"></i></span>' : '<span class="label bg-inverse  label-icon"><i class="fa fa-envelope-o"></i></span>';
				break;
			default:
				$hasil 		= ($type==null) ? '<span class="label label-default  label-icon"><i class="fa fa-question"></i></span>' : '<span class="label label-default  label-icon"><i class="fa fa-question"></i></span>';
				# code...
				break;
		}

		return $hasil;
	}
}

if ( ! function_exists('toTime'))
{
	function toTime($date){
		$time=strtotime($date);
		return date("H:i A",$time);
	}
}

// ------------------------------------------------------------------------
if (!function_exists('tgl_indo')){
	function tgl_indo($tgl){
     	$tanggal = substr($tgl,8,2);
     	switch (substr($tgl,5,2)){
					case '01': 
						$bulan= "Januari";
						break;
					case '02':
						$bulan= "Februari";
						break;
					case '03':
						$bulan= "Maret";
						break;
					case '04':
						$bulan= "April";
						break;
					case '05':
						$bulan= "Mei";
						break;
					case '06':
						$bulan= "Juni";
						break;
					case '07':
						$bulan= "Juli";
						break;
					case '08':
						$bulan= "Agustus";
						break;
					case '09':
						$bulan= "September";
						break;
					case '10':
						$bulan= "Oktober";
						break;
					case '11':
						$bulan= "November";
						break;
					case '12':
						$bulan= "Desember";
						break;
				}

		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;
     }
}

if (!function_exists('tgltobulan_indo')){
	function goObject($var){
		return json_decode(json_encode($var));
	}
}

if (!function_exists('tgltobulan_indo')){
	function tgltobulan_indo($tgl){
		if($tgl==null){
			return false;
		}
     	$tanggal = substr($tgl,8,2);
     	switch (substr($tgl,5,2)){
					case '01': 
						$bulan= "Januari";
						break;
					case '02':
						$bulan= "Februari";
						break;
					case '03':
						$bulan= "Maret";
						break;
					case '04':
						$bulan= "April";
						break;
					case '05':
						$bulan= "Mei";
						break;
					case '06':
						$bulan= "Juni";
						break;
					case '07':
						$bulan= "Juli";
						break;
					case '08':
						$bulan= "Agustus";
						break;
					case '09':
						$bulan= "September";
						break;
					case '10':
						$bulan= "Oktober";
						break;
					case '11':
						$bulan= "November";
						break;
					case '12':
						$bulan= "Desember";
						break;
				}

		$tahun = substr($tgl,0,4);
		return $bulan.' '.$tahun;
     }
}

if (!function_exists('bln_indo')){
	function bln_indo($bln){
     	switch ($bln){
			case '1': 
				$bulan= "Januari";
				break;
			case '2':
				$bulan= "Februari";
				break;
			case '3':
				$bulan= "Maret";
				break;
			case '4':
				$bulan= "April";
				break;
			case '5':
				$bulan= "Mei";
				break;
			case '6':
				$bulan= "Juni";
				break;
			case '7':
				$bulan= "Juli";
				break;
			case '8':
				$bulan= "Agustus";
				break;
			case '9':
				$bulan= "September";
				break;
			case '10':
				$bulan= "Oktober";
				break;
			case '11':
				$bulan= "November";
				break;
			case '12':
				$bulan= "Desember";
				break;
		}
		return $bulan;
     }
}

if (!function_exists('dateRange')){
	function dateRange($strDateFrom,$strDateTo)
	{
	    $aryRange=array();

	    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	    if ($iDateTo>=$iDateFrom)
	    {
	        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
	        while ($iDateFrom<$iDateTo)
	        {
	            $iDateFrom+=86400; // add 24 hours
	            array_push($aryRange,date('Y-m-d',$iDateFrom));
	        }
	    }
	    return $aryRange;
	}
}


