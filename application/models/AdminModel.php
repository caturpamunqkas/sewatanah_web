<?php

class AdminModel extends MY_Model
{
	protected $table 	= "tb_admin";
	protected $appends 	= array('imagedir','url','urlupdate','urldelete');

	public function getImagedirAttribute()
	{
		if (!$this->image|| !file_exists("assets/images/user_picture/{$this->foto}")) {
			return img_holder();
		}

		return base_url("assets/images/user_picture/{$this->foto}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("admin/user/view/{$this->id}");
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/blog/update/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('admin/user/delete/'.$this->id);
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}

}
