<?php

class ConfigModel extends MY_Model
{
	protected $table 	= "tb_config";
	protected $appends 	= array();

	public function getImagedirAttribute()
	{
		if (!$this->image|| !file_exists("images/blog/{$this->image}")) {
			return img_holder();
		}

		return base_url("images/blog/{$this->image}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("admin/user/view/{$this->id}");
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/blog/update/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('admin/user/delete/'.$this->id);
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}

}
