<?php

class FreezeModel extends MY_Model
{
	protected $table 	= "tb_freeze";
	protected $appends 	= array();

	public function user()
	{
		return $this->hasOne('UserModel', 'id_user', 'id_user');
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}

}
