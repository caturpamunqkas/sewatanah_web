<?php

class KategoriModel extends MY_Model
{
	protected $table 	= "tb_kategori";
	protected $appends 	= array('urlupdate','urldelete');

	public function lahan(){
		return $this->hashMany('LahanModel', 'id_kategori', 'id_kategori');
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/blog/update/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('admin/user/delete/'.$this->id);
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}

}
