<?php

class UserModel extends MY_Model
{
	protected $table 	= "tb_user";
	protected $appends 	= array('imagedir','identitydir','url','urlalert','urlidentity','urldelete');

	public function freeze()
	{
		return $this->hasOne('FreezeModel', 'id_user', 'id_user');
	}

	public function lahan()
	{
		return $this->hasMany('LahanModel', 'id_user', 'id_user');
	}

	public function akun()
	{
		return $this->hasOne('AkunModel', 'id_user', 'id_user');
	}

	public function getImagedirAttribute()
	{
		if (!$this->foto && !file_exists("/assets/images/user_picture/{$this->foto}")) {
			return img_holder('profile');
		}

		return base_url("assets/images/user_picture/{$this->foto}");	
	}

	public function getIdentitydirAttribute()
	{
		if (!$this->foto && !file_exists("/assets/images/user_picture/{$this->ktp}")) {
			return img_holder('folder');
		}

		return base_url("assets/images/user_picture/identity/{$this->ktp}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("admin/user/view/{$this->id}");
	}

	public function getUrlalertAttribute()
	{
		return base_url('admin/user/alert/'.$this->id);
	}

	public function getUrlidentityAttribute()
	{
		return base_url('admin/user/verif/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('admin/user/delete/'.$this->id);
	}

	public function scopeNotVerif($query){
		return $query->where("verif",'false');
	}

}
