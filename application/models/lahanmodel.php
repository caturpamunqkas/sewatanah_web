<?php

class LahanModel extends MY_Model
{
	protected $table 	= "tb_lahan";
	protected $appends 	= array('imagedir','url', 'urlstatus','urlupdate','urldelete','urlimage');

	public function user()
	{
		return $this->hasOne('UserModel', 'id_user', 'id_user');
	}

	public function kategori()
	{
		return $this->hasOne('KategoriModel', 'id_kategori', 'id_kategori');
	}

	public function getImagedirAttribute()
	{
		if (!$this->foto_lahan && !file_exists("/assets/images/tanah_picture/{$this->foto_lahan}")) {
			return img_holder();
		}

		return base_url("assets/images/tanah_picture/{$this->foto_lahan}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("admin/lahan/view/{$this->id}");
	}

    public function getUrlStatusAttribute()
	{			
		return base_url("admin/lahan/ubah/{$this->id}");
	}

	public function getUrlupdateAttribute()
	{
		return base_url('admin/lahan/ubah/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('admin/lahan/delete/'.$this->id);
	}

	public function getUrlImageAttribute()
	{
		return base_url('assets/images/tanah_picture/'.$this->foto_lahan);
	}

	public function scopeNotVerif($query){
		return $query->where("status",'false');
	}

}
