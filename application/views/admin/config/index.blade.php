@extends('admin.template')
@section('title', $title)

@section('css')
<!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{base_url("assets")}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

   <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">CK Editor
                <small>Advanced and full of features</small>
              </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                {{-- <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button> --}}
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <form id="form" class="form-horizontal">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama Website<span style="color: red">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" name="nama" id="nama" class="form-control" value="{{$config->nama}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email<span style="color: red">*</span></label>
                    <div class="col-sm-10">
                      <input type="email" id="email" name="email" class="form-control" value="{{$config->email}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Deskripsi<span style="color: red">*</span></label>
                    <div class="col-sm-10">
                      <textarea id="editor1" name="deskripsi" rows="10" cols="80">
                        {{$config->deskripsi}}
                      </textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Logo Website<span style="color: red">*</span></label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control" id="logo" name="logo">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Icon Grayscale<span style="color: red">*</span></label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control" id="icon" name="icon">
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-info pull-right">Submit</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('javascript')
<!-- FastClick -->
<script src="{{base_url("assets")}}/bower_components/fastclick/lib/fastclick.js"></script>
<!-- CK Editor -->
<script src="{{base_url("assets")}}/bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{base_url("assets")}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })

  var base_update = "{{base_url('admin/config/websiteSave')}}";
  $().ready(function(){
        $('#form').submit(function(e){
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                'url': base_update,
                'data': $(this).serialize(),
                'success': function(html){
                    swal({
                      title: 'Data Berhasil Di Ubah',
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
            });
        });
    });
</script>
@endsection