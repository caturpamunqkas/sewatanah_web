@extends('admin.template')
@section('title', $title)

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{base_url("assets")}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Lahan
        {{-- <small>advanced tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{base_url("/admin")}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">List Lahan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>JUDUL</th>
                  <th>ALAMAT LAHAN</th>
                  <th>KATEGORI</th>
                  <th>STATUS</th>
                  <th>PEMILIK</th>
                  <th>AKSI</th>
                </tr>
                </thead>
                <tbody>

                @foreach($data as $d => $result)
                <tr align="center">
                  <td>{{$d+1}}</td>
                  <td>{{read_more($result['judul'], 25)}}</td>
                  <td>{{$result['alamat_lahan']}}</td>
                  <td>{{$result->kategori->kategori}}</td>
                  <td>
                    @if ($result['status'] == 'true')
                      <span class="label label-success">Terverifikasi</span>
                    @elseif ($result['status'] == 'false')
                      <span class="label label-primary">Belum Terverifikasi</span> 
                    @else
                       <span class="label label-danger">Diblokir</span>
                    @endif
                  </td>
                  <td>{{$result->user->nama_depan." ".$result->user->nama_belakang}}<br>
                      @if($result->user->status == 'blocked')
                        <span class="label label-danger">User Di Blokir</span>
                      @endif
                  </td>
                  <td>
                    <a href="{{$result->url}}"><button type="button" class="btn btn-success"><i class="fa fa-eye"></i></button></a>
                    <button type="button" onclick="deleteLahan()" class="btn btn-danger" onclick=""><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                @endforeach

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- DataTables -->
<script src="{{base_url("assets")}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{base_url("assets")}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  var base_url_admin = "{{base_url("/admin")}}";
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  function deleteLahan() {
    swal({
    title: 'Apakah Anda Yakin?',
    text: "Ingin Menghapus Data Ini!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus Ini!'
  }).then((result) => {
    if (result.value) {
      swal({
        title: 'Terhapus!',
        text: 'Data Tersebut Berhasil Dihapus.',
        type: 'success',
        confirmButtonText: 'OK'
      }).then((result) => {
        window.location = "{{$result->urldelete}}";
      });
    }
  })
  }

</script>
@endsection