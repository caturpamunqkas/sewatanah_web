@extends('admin.template')
@section('title', 'Lihat Data '.$data['nama_depan'].' '.$data['nama_belakang'])
@section('css')
<style type="text/css">
  
.zoom {
    /*padding: 50px;*/
    /*background-color: green;*/
    transition: transform .8s; /* Animation */
    width: 100%;
    height: 300px;
    padding-left: 50px;
    padding-right: 50px
}

.zoom:hover {
    transform: scale(1.4);
}

.info{
  width: 100%;
  height: 200px;
  text-align: center;

}
</style>
<!-- DataTables -->
<link rel="stylesheet" href="{{base_url("assets")}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Data User
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{base_url("/admin")}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{base_url("/admin/listuser")}}">List User</a></li>
        <li class="active">Data User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @if($data['alert'] >= 5)
        <div class="callout callout-danger">
          <h4>PERINGATAN AKUN!</h4>
          Mendapatkan {{$data['alert']}} Peringatan. Akun TERBLOKIR Oleh Sistem.
        </div>
      @elseif ($data['alert'] >= 3)
        <div class="callout callout-warning">
          <h4>PERINGATAN AKUN!</h4>
          Mendapatkan {{$data['alert']}} Peringatan. Akun <b>DIBEKUKAN</b> sementara.<br>
          Akun Di Bekukan Sejak {{tgl_indo($data->freeze->startOn)}} - {{tgl_indo($data->freeze->endOn)}} <br> <b>{{dateLeft($data->freeze->endOn)}}</b> Hari Lagi.
        </div>
      @elseif($data['alert'] >= 1)
        <div class="callout callout-info">
          <h4>PERINGATAN AKUN!</h4>
          Mendapatkan {{$data['alert']}} Peringatan.
        </div>
      @endif
      <div class="row">
        <div class="col-xs-12">
          {{-- Content --}}
          <div class="col-md-5">
            <div class="box" {{-- style="height: 490px" --}}>
              @if ($data['verif'] == 'true')
                <span class="label label-success">TERVERIFIKASI</span>
              @elseif($data['verif'] == 'false')
                <span class="label label-danger">BELUM TERVERIFIKASI</span>
              @else
                <span class="label label-default">PROSES VERIFIKASI</span>
              @endif
              <div class="box-primary">
                <div class="box-header with-border box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{$data->imagedir}}" alt="User profile picture">

                  <h3 class="profile-username text-center">{{$data['nama_depan']." ".$data['nama_belakang']}}</h3>

                  <p class="text-muted text-center">Bergabung Sejak {{tgl_indo($data['created_at'])}}</p>

                  <ul class="list-group list-group-unbordered">
                    {{-- <li class="list-group-item">
                      <b>NAMA</b> <a class="pull-right">{{$data['nama_depan']." ".$data['nama_belakang']}}</a>
                    </li> --}}
                    <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right">{{$data['email']}}</a>
                    </li>
                    {{-- <li class="list-group-item">
                      <b>FACEBOOK</b> <a class="pull-right">{{read_more($data['facebook'], 30)}}</a>
                    </li> --}}
                    <li class="list-group-item">
                      <b>GENDER</b>
                      @if ($data['gender'] == 'L')
                          <a class="pull-right">Laki - Laki</a>
                      @else
                          <a class="pull-right">Perempuan</a>
                      @endif
                    </li>
                    <li class="list-group-item">
                      <b>ALAMAT</b> <a class="pull-right">{{$data['alamat']}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>TELEPON</b> <a class="pull-right">{{$data['telepon']}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>PROFESI</b> <a class="pull-right">{{$data['profesi']}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>STATUS AKUN</b>
                      @if ($data['alert'] >= 5)
                        <span class="label pull-right label-danger">BLOCK</span>
                      @elseif($data['alert'] >= 3)
                        <span class="label pull-right" style="background-color: #f39c12">DIBEKUKAN</span>
                      @elseif($data['alert'] >= 1)
                        <span class="label pull-right label-default">MENDAPATKAN {{$data['alert']}} PERINGATAN</span>
                      @elseif($data['alert'] >= 0)
                        <span class="label pull-right label-default">BELUM MENDAPATKAN PERINGATAN</span>
                      @endif
                    </li>
                  </ul>
                    @if ($data->status == 'freeze')
                        <button type="submit" class="btn btn-danger btn-block disabled"><b>BERIKAN ALERT</b></button>
                    @elseif($data->status == 'blocked')
                    <div class="col-md-12">
                      <div class="col-md-5">
                        <button type="submit" class="btn btn-danger btn-block"><b>HAPUS AKUN</b></button>
                      </div>
                      <div class="col-md-2">
                        <span>atau</span>
                      </div>
                      <div class="col-md-5">
                        <button type="submit" class="btn btn-success btn-block"><b>PULIHKAN AKUN</b></button>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <span style="font-size: 10px">*Akun akan di <b>HAPUS</b>. Beserta iklan yang sudah User buat.</span>
                      </div>
                      <div class="col-md-6">
                        <span style="font-size: 10px">*Akun akan di <b>PULIHKAN</b> dari BLOKIR, Iklan yang sudah user buat juga akan di PULIHKAN dari BLOKIR.</span>
                      </div>
                    </div>
                    @else
                      <form id="form" method="post">
                        <button type="submit" class="btn btn-danger btn-block"><b>BERIKAN ALERT</b></button>
                      </form>
                    @endif
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="box" style="height: 490px">
              <div class="box-primary">
                <div class="box-header">
                  <h3 class="box-title">IDENTITAS USER</h3>
                </div>
                <div class="box-body">
                  @if ($data['foto'] != null)
                    {{-- expr --}}
                    <img class="zoom" src="{{$data['identitydir']}}">
                    <center>
                    <div class="container-fluid" style="padding-top: 90px">
                      <div class="col-md-12">
                        <div class="col-md-6">
                          <form id="form1">
                            <input type="text" hidden id="verif" name="verif" value="false">
                            <button type="submit" class="btn btn-danger btn-block">BATALKAN VERIFIKASI</button>
                          </form>
                        </div>
                        <div class="col-md-6">
                          <form id="form1">
                            <input type="text" hidden id="verif" name="verif" value="true">
                            <button type="submit" class="btn btn-success">VERIFIKASI AKUN</button>
                          </form>
                        </div>
                      </div>
                    </div>
                    </center>
                  @else
                    <div class="info">
                      <img class="zoom" src="{{base_url("assets/images/user_picture")}}/{{$data['ktp']}}">
                      <h3>User belum mengirim identitas berupa Scan KTP.</h3>
                    </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="box-primary">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>NO</th>
                      <th>Gambar</th>
                      <th>Tanggal Posting</th>
                      <th>Judul</th>
                      <th>Deskripsi</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody align="center">

                    @foreach($lahan as $d => $result)
                    <tr>
                      <td>{{$d+1}}</td>
                      <td>
                        <span class="info-box-icon" style="width: 150px; background: white">
                          <img style="height: auto;" src="{{$result['imagedir']}}">
                        </span>
                      </td>
                      <td><span class="label label-info">{{tgl_indo($result['created_at'])}}</span></td>
                      <td>{{$result['judul']}}</td>
                      <td>{{$result['deskripsi']}}</td>
                      <td>
                        @if ($result['status']=='true')
                          <img style="margin-left: 3px; margin-bottom: 3px" width="25px" src="{{base_url('assets/dist/img/success.svg')}}"/>
                          <a href="{{$result['url']}}"><p><span class="label label-success">Terverifikasi</span></p></a>
                        @elseif ($result['status']=='false')
                          <img style="margin-left: 3px; margin-bottom: 3px" width="25px" src="{{base_url('assets/dist/img/cancel.svg')}}"/>
                          <a href="{{$result['url']}}"><p><span class="label label-danger">Belum Terverifikasi</span></p></a>
                        @else
                          <img style="margin-left: 3px; margin-bottom: 3px" width="25px" src="{{base_url('assets/dist/img/cancel.svg')}}"/>
                          <a href="{{$result['url']}}"><p><span class="label label-danger">Diblokir</span></p></a>
                        @endif
                      </td>
                    </tr>
                    @endforeach

                  </table>
                </div>
                <!-- /.box-body -->
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- END CONTENT --}}
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{base_url("assets")}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{base_url("assets")}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
var base_alert = "{{$data['urlalert']}}";
var base_identity = "{{$data['urlidentity']}}";

  $().ready(function(){
        $('#form').submit(function(e){
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                'url': base_alert,
                'data': $(this).serialize(),
                'success': function(html){
                    swal({
                      title: 'Berhasil Memberikan Alert',
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
            });
        });
    });

  $().ready(function(){
        $('#form1').submit(function(e){
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                'url': base_identity,
                'data': $(this).serialize(),
                'success': function(html){
                    swal({
                      title: 'Data Berhasil Di Ubah',
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
            });
        });
    });

</script>
@endsection