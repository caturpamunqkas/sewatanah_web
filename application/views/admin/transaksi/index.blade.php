@extends('admin.template')
@section('title', $title)

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{base_url("assets")}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pengguna
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{base_url("/admin")}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">List User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>PEMILIK LAHAN</th>
                  <th>PENYEWA</th>
                  <th>HARGA SEWA</th>
                  <th>LUAS LAHAN</th>
                  <th>STATUS</th>
                  <th>AKSI</th>
                </tr>
                </thead>
                <tbody>

                @foreach($data as $d => $result)
                <tr align="center">
                  <td>{{$d+1}}</td>
                  <td>{{$result->pemilik->nama_depan.' '.$result->pemilik->nama_belakang}}</td>
                  <td>{{$result->penyewa->nama_depan.' '.$result->penyewa->nama_belakang}}</td>
                  <td>Rp. {{$result->lahan->harga}}</td>
                  <td>{{$result->lahan->luas}}</td>
                  <td>
                    @if ($result->status=='true')
                        <p><span class="label label-success">Succes</span></p>
                    @else
                        <p><span class="label label-danger">Memerlukan Verifikasi</span></p>                    
                    @endif
                  </td>
                  <td>
                      <a href="{{$result->url}}"><button class="btn btn-success"><i class="fa fa-eye"></i></button></a>
                    <button type="button" class="btn btn-danger" onclick="deleteUser()"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                @endforeach

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- DataTables -->
<script src="{{base_url("assets")}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{base_url("assets")}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  var base_url_admin = "{{base_url("/admin")}}";
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  function deleteUser() {
    swal({
    title: 'Apakah Anda Yakin?',
    text: "Ingin Menghapus Data Ini!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus Ini!'
  }).then((result) => {
    if (result.value) {
      swal({
        title: 'Terhapus!',
        text: 'Dengan Anda Klik OK, Maka Data Akan Benar-Benar Terhapus.',
        type: 'success',
        confirmButtonText: 'OK'
      }).then((result) => {
        window.location = "{{$result->urldelete}}";
      });
    }
  })
  }

</script>
@endsection