@extends('admin.template')
@section('title', 'Lihat Data '.$data['nama_depan'].' '.$data['nama_belakang'])
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{base_url("assets")}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Data User
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{base_url("/admin")}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{base_url("/admin/listuser")}}">List User</a></li>
        <li class="active">Data User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          {{-- Data Tanah --}}
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title" style="text-align: center">DATA LAHAN</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="col-md-4">                  
                  <img width="300px" src="{{base_url("assets/images/tanah_picture")}}/{{$data->lahan->foto_lahan}}">
                  <hr style="margin-top: 10px; margin-bottom: 10px">
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Lokasi</strong>
                  <p class="text-muted">{{$data->lahan->alamat_lahan}}</p>
                </div>
                <div class="col-md-8">
                  <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>
                  <hr>
                  <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                  <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                  </p>
                  <hr>
                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          {{-- Pemilik Tanah --}}
          <div class="col-md-4">
            <div class="box">
              <div class="box-primary">
                {{-- <p style="text-align: center; font-weight: bold; background: black; color: white">PEMILIK TANAH</p> --}}
                <div class="box-header with-border box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{base_url("assets/images/user_picture")}}/{{$data->pemilik->foto}}" alt="User profile picture">

                  <h3 class="profile-username text-center">{{$data->pemilik->nama_depan." ".$data->pemilik->nama_belakang}}</h3>

                  <p class="text-muted text-center">Bergabung Sejak {{tgl_indo($data->pemilik->created_at)}}</p>

                  <ul class="list-group list-group-unbordered">
                    {{-- <li class="list-group-item">
                      <b>NAMA</b> <a class="pull-right">{{$data['nama_depan']." ".$data['nama_belakang']}}</a>
                    </li> --}}
                    <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right">{{$data->pemilik->email}}</a>
                    </li>
                    {{-- <li class="list-group-item">
                      <b>FACEBOOK</b> <a class="pull-right">{{read_more($data['facebook'], 30)}}</a>
                    </li> --}}
                    <li class="list-group-item">
                      <b>GENDER</b>
                      @if ($data['gender'] == 'L')
                          <a class="pull-right">Laki - Laki</a>
                      @else
                          <a class="pull-right">Perempuan</a>
                      @endif
                    </li>
                    <li class="list-group-item">
                      <b>ALAMAT</b> <a class="pull-right">{{$data->pemilik->alamat}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>TELEPON</b> <a class="pull-right">{{$data->pemilik->telepon}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>PROFESI</b> <a class="pull-right">{{$data->pemilik->profesi}}</a>
                    </li>
                    {{-- <li class="list-group-item">
                      @if ($data->akun->status=='true')
                        <b>AKUN LAHAN</b>
                        <span class="label label-success pull-right">Aktif</span>
                      @else
                        <span class="label label-danger pull-right">Belum Aktif</span>                  
                      @endif
                    </li> --}}
                  </ul>

                  <a href="#" class="btn btn-success btn-block"><b>PEMILIK LAHAN</b></a>
                </div>
              </div>
            </div>
          </div>
          {{-- Data Tanah --}}
          <div class="col-md-4">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title" style="text-align: center">DATA LAHAN</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <img width="300px" src="{{base_url("assets/images/tanah_picture")}}/{{$data->lahan->foto_lahan}}">
                <hr>
                <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>
                <hr>
                <strong><i class="fa fa-map-marker margin-r-5"></i> Lokasi</strong>
                <p class="text-muted">Malibu, California</p>
                <hr>
                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                <p>
                  <span class="label label-danger">UI Design</span>
                  <span class="label label-success">Coding</span>
                  <span class="label label-info">Javascript</span>
                  <span class="label label-warning">PHP</span>
                  <span class="label label-primary">Node.js</span>
                </p>
                <hr>
                <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          {{-- Penyewa Tanah --}}
          <div class="col-md-4">
            <div class="box">
              <div class="box-primary">
                <div class="box-header with-border box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{base_url("assets/images/user_picture")}}/{{$data->penyewa->foto}}" alt="User profile picture">

                  <h3 class="profile-username text-center">{{$data->penyewa->nama_depan." ".$data->penyewa->nama_belakang}}</h3>

                  <p class="text-muted text-center">Bergabung Sejak {{tgl_indo($data->penyewa->created_at)}}</p>

                  <ul class="list-group list-group-unbordered">
                    {{-- <li class="list-group-item">
                      <b>NAMA</b> <a class="pull-right">{{$data['nama_depan']." ".$data['nama_belakang']}}</a>
                    </li> --}}
                    <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right">{{$data->penyewa->email}}</a>
                    </li>
                    {{-- <li class="list-group-item">
                      <b>FACEBOOK</b> <a class="pull-right">{{read_more($data['facebook'], 30)}}</a>
                    </li> --}}
                    <li class="list-group-item">
                      <b>GENDER</b>
                      @if ($data['gender'] == 'L')
                          <a class="pull-right">Laki - Laki</a>
                      @else
                          <a class="pull-right">Perempuan</a>
                      @endif
                    </li>
                    <li class="list-group-item">
                      <b>ALAMAT</b> <a class="pull-right">{{$data->penyewa->alamat}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>TELEPON</b> <a class="pull-right">{{$data->penyewa->telepon}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>PROFESI</b> <a class="pull-right">{{$data->penyewa->profesi}}</a>
                    </li>
                    {{-- <li class="list-group-item">
                      @if ($data->akun->status=='true')
                        <b>AKUN LAHAN</b>
                        <span class="label label-success pull-right">Aktif</span>
                      @else
                        <span class="label label-danger pull-right">Belum Aktif</span>                  
                      @endif
                    </li> --}}
                  </ul>

                  <a href="#" class="btn btn-success btn-block"><b>PENYEWA LAHAN</b></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- END CONTENT --}}
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{base_url("assets")}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{base_url("assets")}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
@endsection