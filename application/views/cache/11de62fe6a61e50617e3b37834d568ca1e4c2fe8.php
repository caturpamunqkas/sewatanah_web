<?php $__env->startSection('title', 'SEWA TANAH'); ?>

<?php $__env->startSection('content'); ?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
.carousel{
    background: #2f4357;
    margin-top: 2px;
}
.carousel .item{
    min-height: 280px; /* Prevent carousel from being distorted if for some reason image doesn't load */
}
.carousel .item img{
    margin: 0 auto; /* Align slide image horizontally center */
}
.bs-example{
	margin: 20px;
}
</style>

<div class="bs-example">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="/examples/images/slide1.png" alt="First Slide">
            </div>
            <div class="item">
                <img src="/examples/images/slide2.png" alt="Second Slide">
            </div>
            <div class="item">
                <img src="/examples/images/slide3.png" alt="Third Slide">
            </div>
        </div>
        <!-- Carousel controls -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
		
		<div class="container">
		<h2 class="slider-title">
			<span>Kategori</span>
		</h2>
		</div>
		<div class="owl-carousel owl-theme manual featured-products-carousel">
			<div class="product">
				<div class="product-details-area">
					<div class="product-ratings">
					<div class="product-actions">
					</div>
				</div>
			</div>
			

	<div class="container mb-xlg">
		<h2 class="slider-title">
			<span class="inline-title">Pilih Brand Favorit Anda</span>
			<span class="line"></span>
		</h2>
		<div class="owl-carousel owl-theme" data-plugin-options="{'items':4, 'loop': false, 'nav': true, 'dots': false, 'margin': 10}">
			<div class="cat-box">
				<a href="javascript:void(0)">
					<img src="<?php echo e(base_url("assets/user")); ?>/img/demos/shop/category/category1.jpg" alt="Category">
				</a>
				<h3>Adidas</h3>
			</div>
			<div class="cat-box">
				<a href="javascript:void(0)">
					<img src="<?php echo e(base_url("assets/user")); ?>/img/demos/shop/category/category2.jpg" alt="Category">
				</a>
				<h3>Nike</h3>
			</div>
			<div class="cat-box">
				<a href="javascript:void(0)">
					<img src="<?php echo e(base_url("assets/user")); ?>/img/demos/shop/category/category3.jpg" alt="Category">
				</a>
				<h3>Polo</h3>
			</div>
			<div class="cat-box">
				<a href="javascript:void(0)">
					<img src="<?php echo e(base_url("assets/user")); ?>/img/demos/shop/category/category4.jpg" alt="Category">
				</a>
				<h3>Baby K</h3>
			</div>
			<div class="cat-box">
				<a href="javascript:void(0)">
					<img src="<?php echo e(base_url("assets/user")); ?>/img/demos/shop/category/category4.jpg" alt="Category">
				</a>
				<h3>Little Bird</h3>
			</div>
			<div class="cat-box">
				<a href="javascript:void(0)">
					<img src="<?php echo e(base_url("assets/user")); ?>/img/demos/shop/category/category4.jpg" alt="Category">
				</a>
				<h3>Trumpette</h3>
			</div>
		</div>
	</div>

	
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>