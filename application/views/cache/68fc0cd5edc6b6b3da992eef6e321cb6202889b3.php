<?php $__env->startSection('title', 'Login User'); ?>


<?php $__env->startSection('content'); ?>
<div role="main" class="main">
<section class="form-section">
	<div class="container">
		<h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Masuk atau Buat Akun Baru</h1>
		<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
			<div class="box-content">
				<form action="#">
					<div class="row">
						<div class="col-md-6">
							<div class="form-content">
								<h3 class="heading-text-color font-weight-normal">Buat Akun Baru</h3>
								<p>Dapatkan banyak promo dan diskon dengan menjadi member BumbeeCollection.</p>
							</div>
							<div class="form-action clearfix">
								<a href="<?php echo e(base_url('user/register')); ?>"><button class="btn btn-primary">Buat Akun Baru</button></a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-content">
								<h3 class="heading-text-color font-weight-normal">Masuk Member</h3>
								<p>Sudah menjadi member? Silakan masuk di sini.</p>
								<div class="form-group">
									<label class="font-weight-normal">Alamat Email <span class="required">*</span></label>
									<input type="text" name="login_email" class="form-control" required>
								</div>
								<div class="form-group">
									<label class="font-weight-normal">Kata Sandi <span class="required">*</span></label>
									<input type="password" name="login_password" class="form-control" required>
								</div>
							</div>
							<div class="form-action clearfix">
								<a href="javascript:void(0)" class="pull-left">Lupa Kata Sandi?</a>
								<button type="submit" class="btn btn-primary">Masuk</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>