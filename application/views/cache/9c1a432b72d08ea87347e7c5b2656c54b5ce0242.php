<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Aksamedia.co.id">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(base_url("assets/user")); ?>/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo e(base_url("assets/user")); ?>/img/apple-touch-icon.png">

	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light">

	<!-- Vendor CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/animate/animate.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/magnific-popup/magnific-popup.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url('assets/user')); ?>/css/revslider.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/theme-elements.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/theme-blog.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/vendor/rs-plugin/css/navigation.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/skins/skin-shop-7.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/demos/demo-shop-7.css">

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(base_url("assets/user")); ?>/css/custom.css">

	<!-- CSS -->
	<?php echo $__env->yieldContent('css'); ?>

	<!-- Head Libs -->
	<script type="text/javascript" src="vendor/modernizr/modernizr.min.js"></script>
			

</head>
<body>
	<header id="header" style="min-height: 0px; margin-bottom: 20px" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 147, 'stickySetTop': '-41px', 'stickyChangeLogo': false}">
			<div class="header-body">
				<div class="header-top">
					<div class="container">
						<!-- Top Left Dropdown -->
						<div class="dropdowns-container">
							<!-- Currency Dropdown -->
							<p class="welcome-msg">Selamat datang, pengunjung!</p>
							<!-- ./Language Dropdown -->
						</div>
						<!-- ./Top Left Dropdown -->

						<!-- Top Right Menu -->
						<div class="top-menu-area">
							<a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)">Menu Lainnya <i class="fa fa-caret-down"></i></a>
							<ul class="top-menu">
								<li><a href="<?php echo e(base_url("auth/user")); ?>">Login</a></li>
								<li><a href="<?php echo e(base_url("auth/user/register")); ?>">Daftar</a></li>
							</ul>
						</div>
						<!-- ./Top Right Menu -->
					</div>
				</div>
			</div>
		</header>
	

			<?php echo $__env->yieldContent('content'); ?>

		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3 margin-at-small-only">
						<div class="row">
						</div>
					</div>
					<div class="col-md-3 margin-at-small-only">
						<div class="row">
							<div class="footer-ribbon">
								<span>Tentang SewaTanah</span>
							</div>
						</div>
						<ul class="links">
							<li><a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)"></a></li>
							<li><a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)"></a></li>
							<li><a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)"></a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<div class="row">
							<div class="footer-ribbon">
								<span>Selalu Update</span>
							</div>
						</div>
						<div class="newsletter">
							<p class="newsletter-info">Selalu up-to-date dengan diskon dan promo dari kami.</p>
							<form id="newsletterForm" action="#" method="POST">
								<div class="input-group">
									<input type="text" name="email" id="email" class="form-control" placeholder="Masukkan Email Anda">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-primary">
											<i class="fa fa-paper-plane"></i>
										</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<div class="container">
					<a href="<?php echo e(base_url("assets/user")); ?>/index.html" class="logo">
						<img src="img/demos/shop/logo-footer.png" class="img-responsive" alt="Logo SewaTanah">
					</a>
					<ul class="social-icons">
						<li class="social-icons-facebook">
							<a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)" target="_blank" title="Sukai Kita di Facebook">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li class="social-icons-twitter">
							<a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)" target="_blank" title="Ikuti Kita di Twitter">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li class="social-icons-linkedin">
							<a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)" target="_blank" title="Ikuti Kita di Linkedin">
								<i class="fa fa-linkedin"></i>
							</a>
						</li>
					</ul>
					<img src="img/demos/shop/payments.png" class="footer-payment" alt="Payments">
					<p class="copyright-text">&copy; 2017. SewaTanahCollection. All Rights Reserved.</p>
				</div>
			</div>
		</footer>
</body>

<!-- Javascript -->
<?php echo $__env->yieldContent('javascript'); ?>
</html>