<?php $__env->startSection('title', 'Daftar User'); ?>

<?php $__env->startSection('content'); ?>
<div role="main" class="main">
	<section class="form-section">
		<div class="container">
			<h1 class="h2 heading-primary font-weight-normal mb-md mt-lg">Buat Akun Baru</h1>
			<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
				<div class="box-content">
					<form action="#">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<strong>Nama Depan</strong>
									<input type="text" name="register_nama" class="form-control" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<strong>Nama Belakang</strong>
										<input type="text" name="register_nama" class="form-control" required>
									</div>
								</div>		
							</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<strong>Email</strong>
									<input type="email" name="register_email" class="form-control" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<strong>Nomor Telepon</strong>
									<input type="telepon" name="register_telepon" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<strong>Jenis Kelamin</strong>
									<div class="radio">
										<label>
											<input type="radio" name="register_gender">
											Laki-Laki
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="register_gender">
											Perempuan
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<strong>Tanggal Lahir</strong>
									
									<input style="padding: inherit;padding-left: 10px;" type="date" name="tanggal lahir" class="form-control" required>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<strong>Kata Sandi</strong>
									<input type="password" name="register_password" class="form-control" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<strong>Ulang Kata Sandi</strong>
									<input type="password" name="register_repassword" class="form-control" required>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="font-weight-normal">
										<input type="checkbox" name="register_newsletter">
										Saya ingin menerima penawaran melalui email
									</label>
								</div>
								<div class="form-group">
									<label class="font-weight-normal">
										<input type="checkbox" name="register_aggrement" required>
										Dengan mengklik tombol "Daftar", Anda telah setuju dengan <a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)">Kebijakan Privasi</a> dan <a href="<?php echo e(base_url("assets/user")); ?>/javascript:void(0)">Syarat & Ketentuan</a>.
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-action clearfix mt-none">
									<a href="<?php echo e(base_url("user/login")); ?>" class="pull-left"><i class="fa fa-angle-double-left"></i> Sudah punya akun? Masuk di sini.</a>
									<button type="submit" class="btn btn-primary">Daftar Akun</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>