<?php $__env->startSection('title', 'Lihat Data '.$data['nama_depan'].' '.$data['nama_belakang']); ?>
<?php $__env->startSection('css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Data User
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo e(base_url("/admin")); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo e(base_url("/admin/listuser")); ?>">List User</a></li>
        <li class="active">Data User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="col-md-4">
            <div class="box">
              <div class="box-primary">
                <div class="box-header with-border box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?php echo e(base_url("assets/user_picture")); ?>/<?php echo e($data['foto']); ?>" alt="User profile picture">

                  <h3 class="profile-username text-center"><?php echo e($data['nama_depan']." ".$data['nama_belakang']); ?></h3>

                  <p class="text-muted text-center">Bergabung Sejak <?php echo e(tgl_indo($data['create_at'])); ?></p>

                  <ul class="list-group list-group-unbordered">
                    
                    <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right"><?php echo e($data['email']); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>FACEBOOK</b> <a class="pull-right"><?php echo e(read_more($data['facebook'], 30)); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>GENDER</b>
                      <?php if($data['gender'] == 'L'): ?>
                          <a class="pull-right">Laki - Laki</a>
                      <?php else: ?>
                          <a class="pull-right">Perempuan</a>
                      <?php endif; ?>
                    </li>
                    <li class="list-group-item">
                      <b>ALAMAT</b> <a class="pull-right"><?php echo e($data['alamat']); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>TELEPON</b> <a class="pull-right"><?php echo e($data['telepon']); ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>PROFESI</b> <a class="pull-right"><?php echo e($data['profesi']); ?></a>
                    </li>
                  </ul>

                  <a href="#" class="btn btn-danger btn-block"><b>NONAKTIFKAN</b></a>
                </div>
              </div>
            </div>
          </div>
          <div class="box-primary">
            <div class="col-md-8">
              
              <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Gambar</th>
                  <th>Judul</th>
                  <th>Deskripsi</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody align="center">

                <?php $__currentLoopData = $lahan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d => $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($d+1); ?></td>
                  <td>
                    <span class="info-box-icon" style="width: 150px; background: white">
                      <img style="height: auto;" src="<?php echo e(base_url("assets/tanah_picture")); ?>/<?php echo e($result['foto_lahan']); ?>">
                    </span>
                  </td>
                  <td><?php echo e($result['judul']); ?></td>
                  <td><?php echo e($result['deskripsi']); ?></td>
                  <td>
                    <?php if($result['status']=='true'): ?>
                        <img style="margin-left: 3px; margin-bottom: 3px" width="25px" src="<?php echo e(base_url('assets/dist/img/success.svg')); ?>"/>
                        <a href="<?php echo e($result['url']); ?>"><p><span class="label label-success">Terverifikasi</span></p></a>
                    <?php else: ?>
                        <img style="margin-left: 3px; margin-bottom: 3px" width="25px" src="<?php echo e(base_url('assets/dist/img/cancel.svg')); ?>"/>
                        <a href="<?php echo e($result['url']); ?>"><p><span class="label label-danger">Belum Terverifikasi</span></p></a>                    
                    <?php endif; ?>
                  </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo e(base_url("assets")); ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>