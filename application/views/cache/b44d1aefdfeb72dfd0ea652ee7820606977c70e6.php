<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('css'); ?>
<style type="text/css">
  .judul{
    font-size: 25px;
    font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  }

  .desc{
    font-size: 18px;
    font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    font-style: italic;
    margin-bottom: 30px
  }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Blank page
      <small>it all starts here</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo e(base_url('admin')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo e(base_url('admin/lahan')); ?>">List Lahan</a></li>
      <li class="active">Data Lahan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body box-profile">
          <center>
            <img width="350px" style="margin-bottom: 0px; margin-top: 10px" class="img-responsive" src="<?php echo e(base_url("assets/tanah_picture")); ?>/<?php echo e($data['foto_lahan']); ?>" alt="User profile picture">
            <span class="judul"><?php echo e($data['judul']); ?></span>
            <p class="desc">"<?php echo e($data['deskripsi']); ?>"</p>
          </center>
          <div class="col-md-6">
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Harga</b> <a class="pull-right"><?php echo e($data["harga"]); ?></a>
              </li>
              
              <li class="list-group-item">
                <b>Irigasi</b> <a class="pull-right"><?php echo e($data['fasilitas_irigasi']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Tanah</b> <a class="pull-right"><?php echo e($data['fasilitas_tanah']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Jalan</b> <a class="pull-right"><?php echo e($data['fasilitas_jalan']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Pemandangan</b> <a class="pull-right"><?php echo e($data['fasilitas_pemandangan']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Status</b> <a class="pull-right">

                  <?php if($data['status'] == 'true'): ?>
                    <span style="font-size: 12px" class="label label-success">Terverifikasi</span>
                  <?php else: ?>
                    <span style="font-size: 12px" class="label label-danger">Belum Terverifikasi</span> 
                  <?php endif; ?>

                </a>
              </li>
              <li class="list-group-item"><center>
                  <?php if($data['status'] == 'true'): ?>
                    <form id="form" method="post">
                      <input type="text" hidden id="status" name="status" value="false">
                      <button type="submit" style="font-size: 16px" class="label btn btn-danger">Batalkan Verifikasi Lahan</button>
                    </form>
                  <?php else: ?>
                    <form id="form" method="post">
                      <input type="text" hidden id="status" name="status" value="true">
                      <button type="submit" style="font-size: 16px" class="label btn btn-success">Verifikasi Lahan</button>
                    </form> 
                  <?php endif; ?></center>
              </li>
            </ul>
          </div>

          <div class="col-md-6">
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Kategori</b> <a class="pull-right"><?php echo e($data["id_kategori"]); ?></a>
              </li>
              <li class="list-group-item">
                <b>Kontak Pemilik</b> <a class="pull-right">543</a>
              </li>
              <li class="list-group-item">
                <b>Luas</b> <a class="pull-right"><?php echo e($data['luas']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Setifikasi</b> <a class="pull-right"><?php echo e($data['sertifikasi']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Kurun Sewa</b> <a class="pull-right"><?php echo e($data['kurun_sewa']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Kondisi</b> <a class="pull-right"><?php echo e($data['kondisi']); ?></a>
              </li>
              <li class="list-group-item">
                <b>Waktu Dibuat</b> <a class="pull-right"><?php echo e($data['fieldCreate_at']); ?></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
var base_update = "<?php echo e($data->Urlupdate); ?>";

  $().ready(function(){
        $('#form').submit(function(e){
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                'url': base_update,
                'data': $(this).serialize(),
                'success': function(html){
                    swal({
                      title: 'Data Berhasil Di Ubah',
                      type: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        location.reload();
                      }
                    })
                }
            });
        });
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>