<div class="col-md-3 col-md-pull-9">
	<aside class="sidebar">
		<h4>{{$user->nama_depan.' '.$user->nama_belakang}}</h4>
		<ul class="nav nav-list">
			<li class="{{match($menu,'index','active')}}"><a href="{{base_url('user')}}">Dashboard</a></li>
			<li class="{{match($menu,'info','active')}}"><a href="{{base_url('user/info')}}">Informasi Pribadi</a></li>
			<li class="{{match($menu,'lahan','active')}}"><a href="{{base_url('user/lahan')}}">Lahan Anda</a></li>
		</ul>
	</aside>
</div>