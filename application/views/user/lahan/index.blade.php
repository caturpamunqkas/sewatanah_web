@extends('user.template')

@section('title', 'Dashboard')

@section('css')
	<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{base_url('assets/user_akun')}}/vendor/select2/select2.css" />
		<link rel="stylesheet" href="{{base_url('assets/user_akun')}}/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
@endsection

@section('content')
<header class="page-header">
	<h2>Lahan</h2>
	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="index.html">
					<i class="fa fa-home"></i>
				</a>
			</li>
			<li><span>Lahan</span></li>
		</ol>
		
		<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
	</div>
</header>
<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
				</div>
		
				<h2 class="panel-title">Basic</h2>
			</header>
			<div class="panel-body">
				<table class="table table-bordered table-striped mb-none" id="datatable-default">
					<thead>
						<tr>
							<th>NO</th>
							<th>GAMBAR</th>
							<th>JUDUL</th>
							<th>ALAMAT LAHAN</th>
							<th>KATEGORI</th>
							<th>STATUS</th>
							<th>AKSI</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data as $element => $result)
						<tr class="gradeX" align="center">
							<td>{{$element+1}}</td>
							<td width="200px" align="center">
								<img width="200px" class="img-responsive" src="{{base_url("assets/images/tanah_picture")}}/{{$result['foto_lahan']}}" alt="User profile picture">
							</td>
							<td>{{$result->judul}}</td>
							<td>{{$result['alamat_lahan']}}</td>
							<td>{{$result->kategori->kategori}}</td>
							<td>
								@if($result['status'] == 'true')
									<span class="label label-success">Terverifikasi</span>
								@elseif($result['status'] == 'false')
									<span class="label label-primary">Belum Terverifikasi</span>
								@else
									<span class="label label-danger">Block</span>
								@endif
							</td>
							<td>
								<button style="font-size: 10px;" type="button" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="licon-pencil"></i> EDIT</button>
								<br>atau<br>
								<button style="font-size: 10px;" type="button" class="mb-xs mt-xs mr-xs btn btn-danger"><i class="licon-trash"></i> HAPUS</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</section>
@endsection

@section('javascript')
<!-- Vendor -->
		<script src="{{base_url('assets/user_akun')}}/vendor/jquery/jquery.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="{{base_url('assets/user_akun')}}/vendor/select2/select2.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="{{base_url('assets/user_akun')}}/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{base_url('assets/user_akun')}}/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="{{base_url('assets/user_akun')}}/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{base_url('assets/user_akun')}}/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="{{base_url('assets/user_akun')}}/javascripts/tables/examples.datatables.default.js"></script>
		<script src="{{base_url('assets/user_akun')}}/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="{{base_url('assets/user_akun')}}/javascripts/tables/examples.datatables.tabletools.js"></script>
@endsection