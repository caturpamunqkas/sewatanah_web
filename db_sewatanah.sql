-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2019 at 08:58 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sewatanah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(60) NOT NULL,
  `create_at` varchar(100) DEFAULT NULL,
  `update_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `id_admin`, `nama`, `username`, `password`, `email`, `create_at`, `update_at`) VALUES
(1, 24, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'super@admin.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_config`
--

CREATE TABLE `tb_config` (
  `id` int(11) NOT NULL,
  `logo` varchar(20) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `deskripsi` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_config`
--

INSERT INTO `tb_config` (`id`, `logo`, `icon`, `nama`, `email`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, '', '', 'SewaTanah', 'dev@sewatanah.com', '                                                                                                                                                                                                                                                                                                                                                                                                                                                <p><em><strong>Lorep Ipsum Dolor Sit Amet</strong></em></p>\r\n\r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      \r\n                      ', '2018-11-09', '2018-12-04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_feedback`
--

CREATE TABLE `tb_feedback` (
  `id_feedback` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `pesan` varchar(300) NOT NULL,
  `create_at` varchar(100) DEFAULT NULL,
  `update_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_feedback`
--

INSERT INTO `tb_feedback` (`id_feedback`, `nama`, `email`, `pesan`, `create_at`, `update_at`) VALUES
(9, 'Dimas Pramudya Sumarsis', 'dimasrajawali76@gmail.com', 'Terima Kasih admin gubuktani.co.id telah memberikan kemudahan dalam mengiklankan , saran lebih ditingkatkan lagi fitur dan tampilannya', '2018-03-22 20:11:58', NULL),
(10, 'Imron rosyadi', 'rosyadi@gmail.com', 'Website ini bagus jarang ada di Indonesia , kalo ada ini yang paling perfect :)', '2018-03-22 20:14:01', NULL),
(11, 'Hendra Fachrur Rojjy', 'hendra123@gmail.com', 'Membantu sekali dalam mengiklankan lahan pertanian saya yang selama ini tidak saya urus, Saran lebih ditingkatkan lagi Thx', '2018-03-22 20:17:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_freeze`
--

CREATE TABLE `tb_freeze` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `startOn` date NOT NULL,
  `endOn` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `create_at` varchar(100) DEFAULT NULL,
  `update_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `id_kategori`, `kategori`, `create_at`, `update_at`) VALUES
(1, 12, 'Pertanian', '2018-01-31 21:13:46', NULL),
(2, 13, 'Perkebunan', '2018-01-31 21:13:54', NULL),
(3, 14, 'Hutan Produksi', '2018-01-31 21:14:07', NULL),
(4, 15, 'Toga', '2018-01-31 21:14:11', NULL),
(5, 16, 'Hidroponik', '2018-01-31 21:15:37', NULL),
(6, 17, 'Tanah Kosong', '2018-01-31 21:15:57', '2018-02-16 13:55:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_lahan`
--

CREATE TABLE `tb_lahan` (
  `id` int(11) NOT NULL,
  `id_lahan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `alamat_lahan` varchar(100) NOT NULL,
  `luas` varchar(7) NOT NULL,
  `sertifikasi` varchar(60) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `kurun_sewa` varchar(10) NOT NULL,
  `status` enum('true','false','block') NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `fasilitas_irigasi` varchar(60) NOT NULL,
  `fasilitas_tanah` varchar(60) NOT NULL,
  `fasilitas_jalan` varchar(60) NOT NULL,
  `fasilitas_pemandangan` varchar(60) NOT NULL,
  `foto_lahan` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lahan`
--

INSERT INTO `tb_lahan` (`id`, `id_lahan`, `id_user`, `id_kategori`, `judul`, `alamat_lahan`, `luas`, `sertifikasi`, `deskripsi`, `harga`, `kurun_sewa`, `status`, `kondisi`, `fasilitas_irigasi`, `fasilitas_tanah`, `fasilitas_jalan`, `fasilitas_pemandangan`, `foto_lahan`, `created_at`, `updated_at`) VALUES
(1, 32, 50, 13, 'Tanah 1', 'Genting Baru', '20', '', 'Tanah Murah Di Daerah Juanda', 200000, 'Pertahun', 'block', '', '', '', '', '', 'LAHAN_32_50.jpg', '2018-11-06', '2019-01-17'),
(2, 33, 50, 13, 'Tanah 2', 'Genting', '200', '', 'Tanah Kosong Disewakan', 2000, 'pertahun', 'block', '', '', '', '', '', 'LAHAN_33_50.jpg', '2018-11-06', '2019-01-17'),
(4, 35, 50, 15, 'Tanah 4', 'Genting Baru', '20', '', 'Tanah Murah Di Daerah Juanda', 200000, 'Pertahun', 'block', '', '', '', '', '', 'LAHAN_32_50.jpg', '2018-11-21', '2019-01-17'),
(5, 35, 50, 16, 'Tanah 5', 'Genting Baru', '20', '', 'Tanah Murah Di Daerah Juanda', 200000, 'Pertahun', 'block', '', '', '', '', '', 'LAHAN_32_50.jpg', '2018-11-06', '2019-01-17');

-- --------------------------------------------------------

--
-- Table structure for table `tb_service`
--

CREATE TABLE `tb_service` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_slider`
--

CREATE TABLE `tb_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `url` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `status` enum('true','false') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_slider`
--

INSERT INTO `tb_slider` (`id`, `name`, `url`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Slider1', '#', 'lahan1.jpg', 'true', '0000-00-00 00:00:00', '2018-11-22 05:24:50'),
(2, 'Slider2', '#', 'lahan3.jpg', 'true', '0000-00-00 00:00:00', '2018-11-22 04:45:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `id_user` int(8) NOT NULL,
  `nama_depan` varchar(15) NOT NULL,
  `nama_belakang` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `gender` enum('L','P') NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `ktp` varchar(100) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `status` enum('null','blocked','freeze') NOT NULL,
  `verif` enum('true','false','proses') NOT NULL,
  `alert` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `id_user`, `nama_depan`, `nama_belakang`, `email`, `password`, `alamat`, `gender`, `telepon`, `ktp`, `foto`, `status`, `verif`, `alert`, `created_at`, `updated_at`) VALUES
(1, 50, 'User', '1', '1@2.3', '21232f297a57a5a743894a0e4a801fc3', 'Genting Tambak Dalam 5', 'L', '121212', 'KTP_50_.png', 'USER_caturpamungkas.png', 'blocked', 'proses', 5, '2018-09-03', '2019-01-17'),
(2, 2, 'User', '2', '12@23.34', '21232f297a57a5a743894a0e4a801fc3', 'Genting Baru', 'P', '085855226808', '', '', 'null', 'false', 0, '2018-11-20', '2018-11-27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_config`
--
ALTER TABLE `tb_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_feedback`
--
ALTER TABLE `tb_feedback`
  ADD PRIMARY KEY (`id_feedback`);

--
-- Indexes for table `tb_freeze`
--
ALTER TABLE `tb_freeze`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`) USING BTREE;

--
-- Indexes for table `tb_lahan`
--
ALTER TABLE `tb_lahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_lahan` (`id_lahan`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indexes for table `tb_service`
--
ALTER TABLE `tb_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_slider`
--
ALTER TABLE `tb_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_config`
--
ALTER TABLE `tb_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_feedback`
--
ALTER TABLE `tb_feedback`
  MODIFY `id_feedback` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_freeze`
--
ALTER TABLE `tb_freeze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_lahan`
--
ALTER TABLE `tb_lahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_service`
--
ALTER TABLE `tb_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_slider`
--
ALTER TABLE `tb_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_lahan`
--
ALTER TABLE `tb_lahan`
  ADD CONSTRAINT `tb_lahan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`),
  ADD CONSTRAINT `tb_lahan_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id_kategori`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
